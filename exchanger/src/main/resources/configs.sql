insert into CONFIGURATION values ('bter.api.url','http://data.bter.com/api/1');
insert into CONFIGURATION values ('pressure.diff.to.trigger.trade','.03');
insert into CONFIGURATION values ('tier.percent.1','1');
insert into CONFIGURATION values ('tier.percent.2', '2');
insert into CONFIGURATION values ('tier.percent.3','3');
insert into CONFIGURATION values ('tier.weighted.divisor.1','1');
insert into CONFIGURATION values ('tier.weighted.divisor.2','2');
insert into CONFIGURATION values ('tier.weighted.divisor.3','3');
insert into CONFIGURATION values ('num.tiers.to.evaluate','3');
insert into CONFIGURATION values ('min.percent.range.to.trade','.02');
insert into CONFIGURATION values ('max.btc.amount','.2');
insert into CONFIGURATION values ('min.btc.trade.value','.01');
insert into CONFIGURATION values ('percent.trigger.sell.for.profit','.03');

