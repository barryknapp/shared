package com.knappbarry.bitcoin.exchanger.strategy.impl;

import java.util.List;

import com.knappbarry.bitcoin.exchanger.clients.ExchangeClient;
import com.knappbarry.bitcoin.exchanger.model.AvailableFunds;
import com.knappbarry.bitcoin.exchanger.model.BookEntry;
import com.knappbarry.bitcoin.exchanger.service.AccountService;
import com.knappbarry.bitcoin.exchanger.service.OrderBookService;
import com.knappbarry.bitcoin.exchanger.service.impl.ConfigurationsServiceImpl;
import com.knappbarry.bitcoin.exchanger.strategy.TradeNarrowSpreadStrategy;

public class TradeNarrowSpreadStrategyImpl implements TradeNarrowSpreadStrategy {

	private OrderBookService orderBookService;
	private AccountService accountService;
	private ConfigurationsServiceImpl configs;
	private ExchangeClient exchangeClient;

	@Override
	public void process(List<String> coinExchanges) throws Exception {

		//CONFIDENTIAL MAGIC ALGORITHMS REMOVED

	}


	private String getStrategy() {
		return "trade.narrow.spread.strategy";
	}

	public void setOrderBookService(OrderBookService orderBookService) {
		this.orderBookService = orderBookService;
	}

	public void setAccountService(AccountService accountService) {
		this.accountService = accountService;
	}

	public void setConfigs(ConfigurationsServiceImpl configs) {
		this.configs = configs;
	}

	public void setExchangeClient(ExchangeClient exchangeClient) {
		this.exchangeClient = exchangeClient;
	}

}
