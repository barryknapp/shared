package com.knappbarry.bitcoin.exchanger.clients;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.knappbarry.bitcoin.exchanger.model.BlockchainInfoCurrency;

/**
 * This is a utility class for performing API calls using GET and POST requests.
 * It is possible to override these calls by implementing HttpClientInterface.
 * Add the new implementation via setCustomHttpClient(...), such that it will
 * get used globally.
 */
public class BlockchainInfoClientImpl implements BlockchainInfoClient {
	private static final String BASE_URL = "https://blockchain.info/";

	public volatile static int TIMEOUT_MS = 10000;

	/**
	 * Perform a GET request on a Blockchain.info API resource.
	 *
	 * @param resource
	 *            Resource path after https://blockchain.info/api/
	 * @param params
	 *            Map containing request parameters
	 * @return String response
	 * @throws APIException
	 *             If the server returns an error
	 */
	private String get(String resource, Map<String, String> params) throws Exception {
		return openURL(BASE_URL, resource, params, "GET");
	}

	private String get(String baseURL, String resource, Map<String, String> params) throws Exception {
		return openURL(baseURL, resource, params, "GET");
	}

	/**
	 * Perform a POST request on a Blockchain.info API resource.
	 *
	 * @param resource
	 *            Resource path after https://blockchain.info/api/
	 * @param params
	 *            Map containing request parameters
	 * @return String response
	 * @throws APIException
	 *             If the server returns an error
	 * @throws IOException
	 *             If the server is not reachable
	 */
	private String post(String resource, Map<String, String> params) throws Exception {
		return openURL(BASE_URL, resource, params, "POST");
	}

	private String post(String baseURL, String resource, Map<String, String> params) throws Exception {
		return openURL(baseURL, resource, params, "POST");
	}

	private String openURL(String baseURL, String resource, Map<String, String> params, String requestMethod)
			throws Exception {
		String encodedParams = urlEncodeParams(params);
		URL url = null;
		IOException ioException = null;

		String responseStr = null;

		if (requestMethod.equals("GET")) {
			if (encodedParams.isEmpty()) {
				url = new URL(baseURL + resource);
			} else {
				url = new URL(baseURL + resource + '?' + encodedParams);
			}
		} else if (requestMethod.equals("POST")) {
			url = new URL(baseURL + resource);
		}

		HttpURLConnection conn = null;
		RuntimeException apiException = null;
		try {
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod(requestMethod);
			conn.setConnectTimeout(TIMEOUT_MS);

			if (requestMethod.equals("POST")) {
				byte[] postBytes = encodedParams.getBytes("UTF-8");
				conn.setDoOutput(true);
				conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				conn.setRequestProperty("Content-Length", String.valueOf(postBytes.length));
				conn.getOutputStream().write(postBytes);
				conn.getOutputStream().close();
			}

			if (conn.getResponseCode() != 200) {
				apiException = new RuntimeException(inputStreamToString(conn.getErrorStream()));
			} else {
				responseStr = inputStreamToString(conn.getInputStream());
			}
		} catch (IOException e) {
			ioException = e;
		} finally {
			try {
				if (apiException != null) {
					conn.getErrorStream().close();
				}
				conn.getInputStream().close();
			} catch (Exception ex) {
			}

			if (ioException != null) {
				throw ioException;
			}

			if (apiException != null) {
				throw apiException;
			}
		}

		return responseStr;
	}

	private String urlEncodeParams(Map<String, String> params) {
		String result = "";

		if (params != null && params.size() > 0) {
			try {
				StringBuilder data = new StringBuilder();
				for (Entry<String, String> kvp : params.entrySet()) {
					if (data.length() > 0) {
						data.append('&');
					}

					data.append(URLEncoder.encode(kvp.getKey(), "UTF-8"));
					data.append('=');
					data.append(URLEncoder.encode(kvp.getValue(), "UTF-8"));
				}
				result = data.toString();
			} catch (UnsupportedEncodingException e) {
			}
		}

		return result;
	}

	private String inputStreamToString(InputStream is) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));

		StringBuilder responseStringBuilder = new StringBuilder();
		String line = "";

		while ((line = reader.readLine()) != null) {
			responseStringBuilder.append(line);
		}

		reader.close();

		return responseStringBuilder.toString();
	}

	/**
	 * Gets the price ticker from https://blockchain.info/ticker
	 *
	 * @param apiCode
	 *            Blockchain.info API code (optional, nullable)
	 * @return A map of currencies where the key is a 3-letter currency symbol
	 *         and the value is the `Currency` class
	 * @throws APIException
	 *             If the server returns an error
	 */
	@Override
	public Map<String, BlockchainInfoCurrency> getTicker() throws Exception {
		Map<String, String> params = new HashMap<String, String>();

		String response = get("ticker", params);
		JsonObject ticker = new JsonParser().parse(response).getAsJsonObject();

		Map<String, BlockchainInfoCurrency> resultMap = new HashMap<String, BlockchainInfoCurrency>();
		for (Entry<String, JsonElement> ccyKVP : ticker.entrySet()) {
			JsonObject ccy = ccyKVP.getValue().getAsJsonObject();
			BlockchainInfoCurrency currency = new BlockchainInfoCurrency(ccy.get("buy").getAsDouble(),
					ccy.get("sell").getAsDouble(), ccy.get("last").getAsDouble(), ccy.get("15m").getAsDouble(),
					ccy.get("symbol").getAsString());

			resultMap.put(ccyKVP.getKey(), currency);
		}

		return resultMap;
	}



	/**
	 * Converts x value in the provided currency to BTC.
	 *
	 * @param currency
	 *            Currency code
	 * @param value
	 *            Value to convert
	 * @param apiCode
	 *            Blockchain.info API code (optional, nullable)
	 * @return Converted value in BTC
	 * @throws APIException
	 *             If the server returns an error
	 */
	@Override
	public BigDecimal toBTC(String currency, BigDecimal value) throws Exception {
		Map<String, String> params = new HashMap<String, String>();
		params.put("currency", currency);
		params.put("value", String.valueOf(value));

		String response = get("tobtc", params);
		return new BigDecimal(response);
	}
}
