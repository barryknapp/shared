package com.knappbarry.bitcoin.exchanger.json.model.bter;

import java.util.Map;

public class AvailableFundsJson {

	boolean result;
	Map<String, Double> available_funds;
	Map<String, Double> locked_funds;
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public Map<String, Double> getAvailable_funds() {
		return available_funds;
	}
	public void setAvailable_funds(Map<String, Double> available_funds) {
		this.available_funds = available_funds;
	}
	public Map<String, Double> getLocked_funds() {
		return locked_funds;
	}
	public void setLocked_funds(Map<String, Double> locked_funds) {
		this.locked_funds = locked_funds;
	}
	@Override
	public String toString() {
		return "AvailableFundsJson [result=" + result + ", available_funds="
				+ available_funds + ", locked_funds=" + locked_funds + "]";
	}
	


	
	
}
