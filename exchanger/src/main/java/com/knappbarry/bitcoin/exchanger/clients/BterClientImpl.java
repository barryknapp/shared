package com.knappbarry.bitcoin.exchanger.clients;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.cfg.NotYetImplementedException;

import com.knappbarry.bitcoin.exchanger.OrderStatusEnum;
import com.knappbarry.bitcoin.exchanger.json.BterJsonToObjectConverter;
import com.knappbarry.bitcoin.exchanger.json.model.bter.OrderJson;
import com.knappbarry.bitcoin.exchanger.json.model.bter.OrderStatusJson;
import com.knappbarry.bitcoin.exchanger.model.AccountConfiguration;
import com.knappbarry.bitcoin.exchanger.model.AvailableFunds;
import com.knappbarry.bitcoin.exchanger.model.BookEntry;
import com.knappbarry.bitcoin.exchanger.model.OrderResponse;
import com.knappbarry.bitcoin.exchanger.model.OrderStatus;
import com.knappbarry.bitcoin.exchanger.service.RestConnectionService;

public class BterClientImpl implements ExchangeClient {

	RestConnectionService restConnectionService;
	BterJsonToObjectConverter jsonToObjectConverter;

	@Override
	public AvailableFunds getAvailableFunds(String strategy, final AccountConfiguration accountConfig) throws Exception {
		String json = restConnectionService.connect("/private/getfunds", "POST", strategy);
		return jsonToObjectConverter.deserializeAvailableFunds(json);
	}

	@Override
	public OrderStatus cancelOrder(String orderid, String strategy, final AccountConfiguration accountConfig) throws Exception {
		Map<String, String> params = new HashMap<String, String>();
		params.put("order_id", orderid);
		String json = restConnectionService.connect("/private/cancelorder", "POST", params, strategy);
		OrderStatusJson orderStatusJson =  jsonToObjectConverter.deserializeOrderStatus(json);
		if(!orderStatusJson.isResult() || orderStatusJson.getOrder()==null){
			return null;
		}
		return convert(orderStatusJson.getOrder());
	}

	@Override
	public OrderStatus getSubmittedOrder(String orderid, String strategy, final AccountConfiguration accountConfig) throws Exception {
		Map<String, String> params = new HashMap<String, String>();
		params.put("order_id", orderid);
		String json = restConnectionService.connect("/private/getorder", "POST", params, strategy);
		OrderStatusJson orderStatusJson =  jsonToObjectConverter.deserializeOrderStatus(json);
		if(!orderStatusJson.isResult() || orderStatusJson.getOrder()==null){
			return null;
		}
		return convert(orderStatusJson.getOrder());
	}

	private OrderStatus convert(OrderJson o) {
		OrderStatus orderStatus = new OrderStatus();

		orderStatus.setStatus(OrderStatusEnum.valueOf(o.getStatus().toLowerCase()));
		orderStatus.setInitial_rate(o.getInitial_rate());
		orderStatus.setAmount(o.getInitial_amount());
		orderStatus.setAmountRemaining(o.getAmount());
		return orderStatus;
	}

	public void setRestConnectionService(RestConnectionService restConnectionService) {
		this.restConnectionService = restConnectionService;
	}

	public void setJsonToObjectConverter(BterJsonToObjectConverter jsonToObjectConverter) {
		this.jsonToObjectConverter = jsonToObjectConverter;
	}

	@Override
	public OrderResponse placeOrder(Map<String, String> params, String strategy, final AccountConfiguration accountConfig) throws Exception {
		String json = restConnectionService.connect("/private/placeorder", "POST", params, strategy);
		OrderResponse orderResponse = jsonToObjectConverter.deserializeOrderResponse(json);
		return orderResponse;
	}

	@Override
	public BookEntry getOrderBook(String coinExchange, String strategy, final AccountConfiguration accountConfig) throws Exception {
		String json = restConnectionService.connect("/depth/" + coinExchange, "GET", strategy);
		return jsonToObjectConverter.deserializeBookEntry(json);
	}

	@Override
	public String getWebsiteName() {
		return "BTER";
	}
	

	@Override
	public List<OrderStatus> getOrderHistory(final AccountConfiguration accountConfig) {
		throw new NotYetImplementedException("not yet implemented");
		
	}

	@Override
	public List<OrderStatus> getOpenOrders(final AccountConfiguration accountConfig) {
		// TODO Auto-generated method stub
		throw new NotYetImplementedException("not yet implemented");
	}

}
