package com.knappbarry.bitcoin.exchanger.service.impl;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.knappbarry.bitcoin.exchanger.OrderStatusEnum;
import com.knappbarry.bitcoin.exchanger.clients.ExchangeClient;
import com.knappbarry.bitcoin.exchanger.dao.CompleteOrderDao;
import com.knappbarry.bitcoin.exchanger.model.AccountConfiguration;
import com.knappbarry.bitcoin.exchanger.model.AvailableFunds;
import com.knappbarry.bitcoin.exchanger.model.CompleteOrder;
import com.knappbarry.bitcoin.exchanger.model.OrderStatus;
import com.knappbarry.bitcoin.exchanger.model.Status;
import com.knappbarry.bitcoin.exchanger.model.TradeType;
import com.knappbarry.bitcoin.exchanger.service.OrderValidationService;

public class OrderValidationServiceImpl implements OrderValidationService {

	private CompleteOrderDao completeOrderDao;
	private ConfigurationsServiceImpl configs;
	private ExchangeClient exchangeClient;

	Logger logger = LogManager.getLogger(OrderValidationServiceImpl.class);

	@Override
	public void validateOrders(AvailableFunds availableFundsInExchange, String strategy,
			final AccountConfiguration accountConfig) throws Exception {

		List<CompleteOrder> openOrdersToCheckForClose = completeOrderDao.findByStatusInAndWebsiteAndAccountKey(
				Arrays.asList(Status.BUY_SUBMIT, Status.SELL_SUBMIT), exchangeClient.getWebsiteName(),
				accountConfig.getAccountKey());
		validateExchangeStatusMatchesDbStatus(openOrdersToCheckForClose, strategy, accountConfig);

	}

	private void validateExchangeStatusMatchesDbStatus(List<CompleteOrder> completedOrders, String strategy,
			final AccountConfiguration accountConfig) throws Exception {
		for (CompleteOrder completeOrder : completedOrders) {
			OrderStatus orderStatusJson;
			if (completeOrder.getOrderId().startsWith("RESTART-")) {
				// the exchange will not have this id, since it was genera
				orderStatusJson = new OrderStatus();
				orderStatusJson.setAmount(completeOrder.getAmount());
				orderStatusJson.setCoin(configs.getCoinForExchange(completeOrder.getExchange()));
				orderStatusJson.setId(completeOrder.getOrderId());
				orderStatusJson.setInitial_rate(completeOrder.getPrice());
				orderStatusJson.setType(TradeType.buy);
				orderStatusJson.setStatus(OrderStatusEnum.closed);
			} else {
				orderStatusJson = exchangeClient.getSubmittedOrder(completeOrder.getOrderId(), strategy, accountConfig);
			}

			if (orderStatusJson == null) {
				continue;
			} else {
				updateClosedOrder(completeOrder, orderStatusJson, strategy, accountConfig);
			}
		}

	}

	private void updateClosedOrder(CompleteOrder completeOrder, OrderStatus orderStatusJson, String strategy,
			final AccountConfiguration accountConfig) throws Exception {

		if (orderStatusJson.getStatus() != OrderStatusEnum.closed) {
			return;
		}

		if (completeOrder.getStatus() == Status.BUY_SUBMIT) {
			completeOrder.setStatus(Status.BUY_CLOSED);
			completeOrder.setPriceConfirmed(orderStatusJson.getInitial_rate());
			logger.info(accountConfig.getAccountKey() + " COMPLETED BUY ORDER VERIFIED CLOSED : " + completeOrder);
		} else if (completeOrder.getStatus() == Status.SELL_SUBMIT) {
			if (completeOrder.getSellOrderId() == null) {
				// there was no sell order id, not sure hwy it was
				// not set to sell submit since we need an order id
				// to track the sell
				completeOrder.setStatus(Status.BUY_SUBMIT);
			} else {
				OrderStatus orderStatusSell = exchangeClient.getSubmittedOrder(completeOrder.getSellOrderId(), strategy,
						accountConfig);
				if (orderStatusSell == null) {
					completeOrder.setSellOrderId(null);
					completeOrder.setStatus(Status.BUY_CLOSED);
				} else if (orderStatusSell.getStatus() == OrderStatusEnum.closed) {
					completeOrder.setSellPriceConfirmed(orderStatusSell.getInitial_rate());
					completeOrder.setStatus(Status.SELL_CLOSED);
					logger.info(
							accountConfig.getAccountKey() + " COMPLETED SELL ORDER VERIFIED CLOSED : " + completeOrder);
				}

			}
		}

		completeOrderDao.save(completeOrder);

	}

	public void setCompleteOrderDao(CompleteOrderDao completeOrderDao) {
		this.completeOrderDao = completeOrderDao;
	}

	public void setConfigs(ConfigurationsServiceImpl configs) {
		this.configs = configs;
	}

	public void setExchangeClient(ExchangeClient exchangeClient) {
		this.exchangeClient = exchangeClient;
	}

}
