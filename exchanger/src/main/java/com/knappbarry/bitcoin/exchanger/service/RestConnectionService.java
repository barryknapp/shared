package com.knappbarry.bitcoin.exchanger.service;

import java.util.Map;

public interface RestConnectionService {

	String connect(String path, String httpVerb, Map<String, String> postParams, String strategy) throws Exception;

	String connect(String path, String httpVerb, String strategy) throws Exception;

}
