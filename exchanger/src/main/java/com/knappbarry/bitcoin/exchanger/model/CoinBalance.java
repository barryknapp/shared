package com.knappbarry.bitcoin.exchanger.model;

public class CoinBalance {

	private String coinSymbol;
	private Double balance;
	public String getCoinSymbol() {
		return coinSymbol;
	}
	public void setCoinSymbol(String coinSymbol) {
		this.coinSymbol = coinSymbol;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	@Override
	public String toString() {
		return "CoinBalance [coinSymbol=" + coinSymbol + ", balance=" + balance
				+ "]";
	}
	
	
}
