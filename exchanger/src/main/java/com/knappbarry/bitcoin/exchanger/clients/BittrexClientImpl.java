package com.knappbarry.bitcoin.exchanger.clients;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.knappbarry.bitcoin.exchanger.OrderStatusEnum;
import com.knappbarry.bitcoin.exchanger.json.BittrexJsonToObjectConverter;
import com.knappbarry.bitcoin.exchanger.json.model.bittrex.OrderJson;
import com.knappbarry.bitcoin.exchanger.json.model.bittrex.OrderStatusJson;
import com.knappbarry.bitcoin.exchanger.model.AccountConfiguration;
import com.knappbarry.bitcoin.exchanger.model.AvailableFunds;
import com.knappbarry.bitcoin.exchanger.model.BookEntry;
import com.knappbarry.bitcoin.exchanger.model.OrderResponse;
import com.knappbarry.bitcoin.exchanger.model.OrderStatus;
import com.knappbarry.bitcoin.exchanger.service.impl.ConfigurationsServiceImpl;

public class BittrexClientImpl implements ExchangeClient {

	public static final String ORDER_LIMIT = "LIMIT", ORDER_MARKET = "MARKET";
	public static final String TRADE_BUY = "BUY", TRADE_SELL = "SELL";
	public static final String TIMEINEFFECT_GOOD_TIL_CANCELLED = "GOOD_TIL_CANCELLED",
			TIMEINEFFECT_IMMEDIATE_OR_CANCEL = "IMMEDIATE_OR_CANCEL", TIMEINEFFECT_FILL_OR_KILL = "FILL_OR_KILL";
	public static final String CONDITION_NONE = "NONE", CONDITION_GREATER_THAN = "GREATER_THAN",
			CONDITION_LESS_THAN = "LESS_THAN", CONDITION_STOP_LOSS_FIXED = "STOP_LOSS_FIXED",
			CONDITION_STOP_LOSS_PERCENTAGE = "STOP_LOSS_PERCENTAGE";
	private static final Exception InvalidStringListException = new Exception("Must be in key-value pairs");
	private final String API_VERSION = "2.0", API_VERSION_1_1 = "1.1", INITIAL_URL = "https://bittrex.com/api/";
	private final String METHOD_PUBLIC = "pub", METHOD_PUBLIC_1_1 = "public", METHOD_KEY = "key";
	private final String MARKET = "market", MARKETS = "markets", CURRENCY = "currency", CURRENCIES = "currencies",
			BALANCE = "balance", ORDERS = "orders";
	private final String encryptionAlgorithm = "HmacSHA512";

	ConfigurationsServiceImpl configs;
	private BittrexJsonToObjectConverter jsonToObjectConverter;

	Logger logger = LogManager.getLogger(BittrexClientImpl.class);

	// public api
	// https://bittrex.com/api/v2.0/pub/markets/getmarketsummaries
	// https://bittrex.com/api/v2.0/pub/currencies/getcurrencies


	public Response getMarketSummaries(final String apiKey, final String apiSecret) { // Returns
																						// a
																						// 24-hour
																						// summary
																						// of
																						// all
		// markets

		return getResponse(METHOD_PUBLIC, MARKETS, "getmarketsummaries", apiKey, apiSecret);
	}

	public Response getCurrencies(final String apiKey, final String apiSecret) { // Returns
																					// all
																					// currencies
																					// currently
																					// on
		// Bittrex with their metadata

		return getResponse(METHOD_PUBLIC, CURRENCIES, "getcurrencies", apiKey, apiSecret);
	}

	public Response getBalanceDistribution(String currency, final String apiKey, final String apiSecret) { // Returns
																											// the
		// balance
		// distribution
		// for a
		// specific
		// currency

		return getResponse(METHOD_PUBLIC, CURRENCY, "getbalancedistribution", API_VERSION,
				returnCorrectMap("currencyname", currency), apiKey, apiSecret);
	}

	public Response getMarketSummary(String market, final String apiKey, final String apiSecret) { // Returns
																									// a
																									// 24-hour
		// summar for a specific
		// market

		return getResponse(METHOD_PUBLIC, MARKET, "getmarketsummary", API_VERSION,
				returnCorrectMap("marketname", market), apiKey, apiSecret);
	}

	public Response getMarketOrderBook(String market, final String apiKey, final String apiSecret) { // Returns
																										// the
																										// orderbook
		// for a specific market

		// 2.0 no good yet --return getResponse(METHOD_PUBLIC, MARKET,
		// "getmarketorderbook", API_VERSION, returnCorrectMap("market", market,
		// "type", "both", "depth","20"));
		return getResponse(METHOD_PUBLIC_1_1, null, "getorderbook", API_VERSION_1_1,
				returnCorrectMap("market", market, "type", "both", "depth", "20"), apiKey, apiSecret);

	}

	public Response getMarketHistory(String market, final String apiKey, final String apiSecret) { // Returns
																									// latest
																									// trades
		// that occurred for a
		// specific market

		return getResponse(METHOD_PUBLIC, MARKET, "getmarkethistory", API_VERSION,
				returnCorrectMap("marketname", market), apiKey, apiSecret);
	}

	public Response getOrder(String orderId, final String apiKey, final String apiSecret) { // Returns
																							// information
																							// about
																							// a
		// specific order (by UUID)

		return getResponse(METHOD_KEY, ORDERS, "getorder", API_VERSION, returnCorrectMap("orderid", orderId), apiKey,
				apiSecret);
	}

	public Response getOpenOrders(final String apiKey, final String apiSecret) { // Returns
																					// all
																					// your
																					// currently
																					// open
																					// orders

		return getResponse(METHOD_KEY, ORDERS, "getopenorders", apiKey, apiSecret);
	}

	public Response getOrderHistoryAllCoins(final String apiKey, final String apiSecret) { // Returns
																							// all
																							// of
																							// your
																							// order
		// history

		return getResponse(null, "account", "getorderhistory", API_VERSION_1_1, returnCorrectMap("count", "100"),
				apiKey, apiSecret);
	}

	public Response cancelOrder(String orderId, final String apiKey, final String apiSecret) {

		return getResponse(null, MARKET, "cancel", API_VERSION_1_1, returnCorrectMap("uuid", orderId), apiKey,
				apiSecret);
	}

	public Response getOpenOrders(String market, final String apiKey, final String apiSecret) { // Returns
																								// your
																								// currently
		// open orders in a specific
		// market

		return getResponse(METHOD_KEY, MARKET, "getopenorders", API_VERSION, returnCorrectMap("marketname", market),
				apiKey, apiSecret);
	}

	public Response getBalances(final String apiKey, final String apiSecret) { // Returns
																				// all
																				// current
																				// balances

		return getResponse(METHOD_KEY, BALANCE, "getbalances", apiKey, apiSecret);
	}

	public Response getBalance(String currency, final String apiKey, final String apiSecret) { // Returns
																								// the
																								// balance
																								// of
																								// a
		// specific currency

		return getResponse(METHOD_KEY, BALANCE, "getbalance", API_VERSION, returnCorrectMap("currencyname", currency),
				apiKey, apiSecret);
	}

	private Response placeOrder(String tradeType, String market, String quantity, String rate, final String apiKey,
			final String apiSecret) {

		String method = null;

		if (tradeType.equals(TRADE_BUY)) {

			method = "buylimit";
		} else if (tradeType.equals(TRADE_SELL)) {
			method = "selllimit";
		}

		return getResponse(null, MARKET, method, API_VERSION_1_1,
				returnCorrectMap("market", market, "quantity", quantity, "rate", rate), apiKey, apiSecret);
	}

	private HashMap<String, String> returnCorrectMap(String... parameters) {

		HashMap<String, String> map = null;

		try {

			map = generateHashMapFromStringList(parameters);

		} catch (Exception e) {

			e.printStackTrace();
		}

		return map;
	}

	private HashMap<String, String> generateHashMapFromStringList(String... strings) throws Exception {
		if (strings.length % 2 != 0)

			throw InvalidStringListException;

		HashMap<String, String> map = new HashMap<String, String>();

		for (int i = 0; i < strings.length; i += 2) // Each key will be i, with
													// the following becoming
													// its value

			map.put(strings[i], strings[i + 1]);

		return map;
	}

	private Response getResponse(String type, String methodGroup, String method, final String apiKey,
			final String apiSecret) {

		return getResponse(type, methodGroup, method, API_VERSION, new HashMap<String, String>(), apiKey, apiSecret);
	}

	private Response getResponse(String type, String methodGroup, String method, String apiVersion,
			HashMap<String, String> parameters, final String apiKey, final String apiSecret) {

		return getResponseBody(generateUrl(type, methodGroup, method, apiVersion, parameters), apiKey, apiSecret);
	}

	private String generateUrl(String type, String methodGroup, String method, String apiVersion,
			HashMap<String, String> parameters) {

		String url = INITIAL_URL;

		url += "v" + apiVersion + "/";
		if (type != null) {
			url += type + "/";
		}
		if (methodGroup != null) {
			url += methodGroup + "/";
		}
		url += method;

		url += generateUrlParameters(parameters);

		return url;
	}

	private String generateUrlParameters(HashMap<String, String> parameters) { // Returns
																				// a
																				// String
																				// with
																				// the
																				// key-value
																				// pairs
																				// formatted
																				// for
																				// URL

		String urlAttachment = "?";

		Object[] keys = parameters.keySet().toArray();

		for (Object key : keys)

			urlAttachment += key.toString() + "=" + parameters.get(key) + "&";

		return urlAttachment;
	}

	private Response getResponseBody(String url, final String apiKey, final String apiSecret) {

		Response response = null;
		boolean publicRequest = true;

		if (!url.substring(url.indexOf("/v")).contains("/" + METHOD_PUBLIC + "/")) {
			url += "apikey=" + apiKey + "&nonce=" + EncryptionUtility.generateNonce();
			publicRequest = false;
		}

		try {
			try {
				// slow it down..sending requests too quickly is getting null
				// responses
				Thread.sleep(500);
			} catch (Exception e) {
			}
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet request = new HttpGet(url);

			if (!publicRequest)

				request.addHeader("apisign", EncryptionUtility.calculateHash(apiSecret, url, encryptionAlgorithm)); // Attaches
																													// signature
																													// as
																													// a
																													// header
			logger.debug("Calling URL " + url);

			HttpResponse httpResponse = client.execute(request);

			int responseCode = httpResponse.getStatusLine().getStatusCode();
			logger.debug("Received response " + responseCode);
			BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));

			StringBuffer resultBuffer = new StringBuffer();
			String line = "";

			while ((line = reader.readLine()) != null)

				resultBuffer.append(line);

			response = createResposeFromUrlResponse(resultBuffer.toString());
			response.setResponseCode(responseCode);

		} catch (UnknownHostException e) {
			logger.error("Hey dummy, check the internet connection!", e);

		} catch (IOException e) {
			logger.error("", e);
		}

		return response;
	}

	private Response createResposeFromUrlResponse(String urlResponse) { // Creates
																		// a new
																		// Response
																		// object
																		// with
																		// the
																		// fields
																		// found
																		// in
																		// the
																		// result

		String successString = "\"success\":";
		int indexOfSuccessString = urlResponse.indexOf(successString) + successString.length();
		try {
			String strSuccess = urlResponse.substring(indexOfSuccessString,
					urlResponse.indexOf(",\"", indexOfSuccessString));

			String resultString = "\"result\":";
			int indexOfResultString = urlResponse.indexOf(resultString) + resultString.length();
			String result = urlResponse.substring(indexOfResultString, urlResponse.lastIndexOf("}"));

			String messageString = "\"message\":\"";
			int indexOfMessageString = urlResponse.indexOf(messageString) + messageString.length();
			String message = urlResponse.substring(indexOfMessageString,
					urlResponse.indexOf("\"", indexOfMessageString));

			boolean success = Boolean.parseBoolean(strSuccess);

			return new Response(success, result, message);
		} catch (Exception e) {
			logger.error("Error parsing response " + urlResponse, e);
			throw new RuntimeException(e);
		}
	}

	public class Response {

		private boolean success;
		private int responseCode;
		private String result;
		private String message;

		private Response(boolean success, int responseCode, String result, String message) {

			this.success = success;
			this.responseCode = responseCode;
			this.result = result;
			this.message = message;
		}

		private Response(boolean success, String result, String message) {

			this.success = success;
			this.result = result;
			this.message = message;
		}

		private void setResponseCode(int responseCode) {

			this.responseCode = responseCode;
		}

		public boolean isSuccessful() {

			return success;
		}

		public String getResult() {

			return result;
		}

		public String getMessage() {

			return message;
		}

		public int getResponseCode() {

			return responseCode;
		}

		@Override
		public String toString() {

			return result;
		}
	}

	@Override
	public AvailableFunds getAvailableFunds(String strategy, final AccountConfiguration accountConfig)
			throws Exception {
		try {
			String json = getBalances(accountConfig.getApiKey(), accountConfig.getApiSecret()).getResult();
			return jsonToObjectConverter.deserializeAvailableFunds(json);
		} catch (Exception e) {
			logger.error("Error getAvailableFunds  " + accountConfig, e);
			return null;
		}
	}

	@Override
	public OrderStatus cancelOrder(String orderid, String strategy, final AccountConfiguration accountConfig)
			throws Exception {
		Response response = cancelOrder(orderid, accountConfig.getApiKey(), accountConfig.getApiSecret());
		if (response.isSuccessful() || (response.getResponseCode() == 200 && "null".equals(response.getResult()))) {
			OrderStatus o = new OrderStatus();
			o.setStatus(OrderStatusEnum.closed);
			return o;
		}
		String json = response.getResult();
		OrderStatusJson orderStatusJson = jsonToObjectConverter.deserializeOrderStatus(json);
		if (!orderStatusJson.isSuccess() || orderStatusJson.getResult() == null) {
			return null;
		}
		return convert(orderStatusJson.getResult());
	}

	@Override
	public OrderStatus getSubmittedOrder(String orderid, String strategy, final AccountConfiguration accountConfig)
			throws Exception {
		String json = getOrder(orderid, accountConfig.getApiKey(), accountConfig.getApiSecret()).getResult();
		OrderJson orderJson = jsonToObjectConverter.deserializeSubmittedOrder(json);
		return convert(orderJson);
	}

	private OrderStatus convert(OrderJson o) {
		if (o == null) {
			return null;
		}
		OrderStatus orderStatus = new OrderStatus();
		if (o.getClosed() != null) {
			if (o.getCancelInitiated()) {
				orderStatus.setStatus(OrderStatusEnum.cancelled);
			} else {
				orderStatus.setStatus(OrderStatusEnum.closed);
			}
		} else {
			orderStatus.setStatus(OrderStatusEnum.open);
		}
		orderStatus.setId(o.getOrderUuid());
		orderStatus.setAmount(o.getQuantity());
		orderStatus.setCoin(o.getExchange().split("-")[1]);
		orderStatus.setInitial_rate(o.getPricePerUnit() == null ? o.getLimit() : o.getPricePerUnit());
		orderStatus.setAmountRemaining(o.getQuantityRemaining());
		return orderStatus;
	}

	@Override
	public OrderResponse placeOrder(Map<String, String> params, String strategy,
			final AccountConfiguration accountConfig) throws Exception {
		String json = placeOrder(params.get("type"), convertPairToMarket(params.get("pair")), params.get("amount"),
				params.get("rate"), accountConfig.getApiKey(), accountConfig.getApiSecret()).getResult();
		OrderResponse orderResponse = jsonToObjectConverter.deserializeOrderResponse(json);
		return orderResponse;
	}

	@Override
	public BookEntry getOrderBook(String coinExchange, String strategy, final AccountConfiguration accountConfig)
			throws Exception {
		String json = getMarketOrderBook(convertPairToMarket(coinExchange), accountConfig.getApiKey(),
				accountConfig.getApiSecret()).getResult();
		return jsonToObjectConverter.deserializeBookEntry(json);
	}

	public void setJsonToObjectConverter(BittrexJsonToObjectConverter jsonToObjectConverter) {
		this.jsonToObjectConverter = jsonToObjectConverter;
	}

	public void setConfigs(ConfigurationsServiceImpl configs) {
		this.configs = configs;
	}

	private String convertPairToMarket(String pair) {
		String[] market = pair.split("_");
		return "BTC-" + market[0].toUpperCase();
	}

	@Override
	public String getWebsiteName() {
		return "BITTREX";
	}

	@Override
	public List<OrderStatus> getOrderHistory(final AccountConfiguration accountConfig) {
		String json = getOrderHistoryAllCoins(accountConfig.getApiKey(), accountConfig.getApiSecret()).getResult();
		return jsonToObjectConverter.deserializeOrderHistory(json);

	}
	
	
	@Override
	public List<OrderStatus> getOpenOrders(final AccountConfiguration accountConfig) {
		String json = getOpenOrders(accountConfig.getApiKey(), accountConfig.getApiSecret()).getResult();
		return jsonToObjectConverter.deserializeOrderHistory(json);
		
	}

}
