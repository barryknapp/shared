package com.knappbarry.bitcoin.exchanger.strategy;

import java.math.BigDecimal;
import java.util.Optional;

public interface BuyHoldSellOnBookPressureStrategy {

	void process(String accountKey, boolean restart, Optional<BigDecimal> priceOfBitcoin) throws Exception;

}
