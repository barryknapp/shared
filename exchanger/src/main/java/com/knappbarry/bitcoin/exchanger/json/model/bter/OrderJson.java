package com.knappbarry.bitcoin.exchanger.json.model.bter;

public class OrderJson {

	private String id;

	private String status;
	private String pair;
	private String type;
	private Object rate;
	//the amount remaining to close
	private Double amount;
	private Double initial_rate;
	//the original trade amount that needs to be traded to close this order
	private Double initial_amount;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPair() {
		return pair;
	}
	public void setPair(String pair) {
		this.pair = pair;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Object getRate() {
		return rate;
	}
	public void setRate(Object rate) {
		this.rate = rate;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getInitial_rate() {
		return initial_rate;
	}
	public void setInitial_rate(Double initial_rate) {
		this.initial_rate = initial_rate;
	}
	public Double getInitial_amount() {
		return initial_amount;
	}
	public void setInitial_amount(Double initial_amount) {
		this.initial_amount = initial_amount;
	}
	@Override
	public String toString() {
		return "OrderJson [id=" + id + ", status=" + status + ", pair=" + pair
				+ ", type=" + type + ", rate=" + rate + ", amount=" + amount
				+ ", initial_rate=" + initial_rate + ", initial_amount="
				+ initial_amount + "]";
	}
	
	
}

