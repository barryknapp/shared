package com.knappbarry.bitcoin.exchanger.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.knappbarry.bitcoin.exchanger.model.Configuration;

public interface ConfigurationsDao extends CrudRepository<Configuration, Integer> {

	
	 public List<Configuration> findByKey(String key);
}
