package com.knappbarry.bitcoin.exchanger.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;

import com.fasterxml.jackson.annotation.JsonFormat;

@AutoProperty
@Entity
@Table(name = "COMPLETE_ORDER")
public class CompleteOrder {

	@Id
	@Column(name = "ORDER_ID")
	private String orderId;

	@Column(name = "SELL_ORDER_ID")
	private String sellOrderId;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm")
	@Column(name = "CREATE_TS")
	private Date createTs;
	@Column(name = "AMOUNT")
	private Double amount;
	@Column(name = "PRICE")
	private Double price;
	@Column(name = "PRICE_CONFIRMED")
	private Double priceConfirmed;
	@Column(name = "SELL_PRICE_CONFIRMED")
	private Double sellPriceConfirmed;
	
	@Column(name = "EXCHANGE")
	private String exchange;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm")
	@Column(name = "UPDATE_TS")
	private Date updateTs;
	@Column(name = "AMOUNT_TO_CLOSE")
	private Double amountToClose;
	@Column(name = "STRATEGY")
	private String strategy;
	@Column(name = "WEBSITE")	
	private String website;
	
	@Column(name = "ACCOUNT_KEY")	
	private String accountKey;

	
	@Enumerated(EnumType.STRING)
	@Column(name = "STATUS")
	private Status status;
	
	
	@Column(name = "CONFIRMED_EXCHANGE_TS")
	private Date confirmedWithExchange;
	
	
	
	public String getAccountKey() {
		return accountKey;
	}

	public void setAccountKey(String accountKey) {
		this.accountKey = accountKey;
	}

	public Date getConfirmedWithExchange() {
		return confirmedWithExchange;
	}

	public void setConfirmedWithExchange(Date confirmedWithExchange) {
		this.confirmedWithExchange = confirmedWithExchange;
	}

	public Double getSellPriceConfirmed() {
		return sellPriceConfirmed;
	}

	public void setSellPriceConfirmed(Double sellPriceConfirmed) {
		this.sellPriceConfirmed = sellPriceConfirmed;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getSellOrderId() {
		return sellOrderId;
	}

	public String getStrategy() {
		return strategy;
	}

	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}

	public Double getAmountToClose() {
		return amountToClose;
	}

	public void setAmountToClose(Double amountToClose) {
		this.amountToClose = amountToClose;
	}

	public Date getUpdateTs() {
		return updateTs;
	}

	public void setUpdateTs(Date updateTs) {
		this.updateTs = updateTs;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Date getCreateTs() {
		return createTs;
	}

	public void setCreateTs(Date createTs) {
		this.createTs = createTs;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getExchange() {
		return exchange;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public Double getPriceConfirmed() {
		return priceConfirmed;
	}

	public void setPriceConfirmed(Double priceConfirmed) {
		this.priceConfirmed = priceConfirmed;
	}

	public void setSellOrderId(String sellOrderId) {
		this.sellOrderId = sellOrderId;
	}

	@Override
	public int hashCode() {
		return Pojomatic.hashCode(this);
	}

	@Override
	public String toString() {
		return Pojomatic.toString(this);
	}

	@Override
	public boolean equals(Object o) {
		return Pojomatic.equals(this, o);
	}

}
