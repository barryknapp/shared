package com.knappbarry.bitcoin.exchanger.model;

import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;

import com.knappbarry.bitcoin.exchanger.OrderStatusEnum;

@AutoProperty
public class OrderStatus {

	private String id;
//	private String sell_type;
//	private String buy_type;
//	private Double sell_amount;
//	private Double buy_amount;
	private String coin;
	private TradeType type;
//	private Double rate;
	private Double amount;
	private Double amountRemaining;
	private Double quantityRemaining;
	private Double initial_rate;
	private Double pricePerUnit;
//	private String initial_amount;
	private OrderStatusEnum status;

	
	public Double getPricePerUnit() {
		return pricePerUnit;
	}
	public void setPricePerUnit(Double pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	}
	public Double getQuantityRemaining() {
		return quantityRemaining!=null ? quantityRemaining : amountRemaining;
	}
	public void setQuantityRemaining(Double quantityRemaining) {
		this.quantityRemaining = quantityRemaining;
	}
	public Double getAmountRemaining() {
		return getQuantityRemaining();
	}
	public void setAmountRemaining(Double amountRemaining) {
		this.amountRemaining = amountRemaining;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getInitial_rate() {
		return initial_rate;
	}
	public void setInitial_rate(Double initial_rate) {
		this.initial_rate = initial_rate;
	}

	public OrderStatusEnum getStatus() {
		return status;
	}
	public void setStatus(OrderStatusEnum status) {
		this.status = status;
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCoin() {
		return coin;
	}
	public void setCoin(String coin) {
		this.coin = coin;
	}
	public TradeType getType() {
		return type;
	}
	public void setType(TradeType type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return Pojomatic.toString(this);
	}

	@Override
	public int hashCode() {
		return Pojomatic.hashCode(this);
	}

	@Override
	public boolean equals(Object o) {
		return Pojomatic.equals(this, o);
	}

	
}

