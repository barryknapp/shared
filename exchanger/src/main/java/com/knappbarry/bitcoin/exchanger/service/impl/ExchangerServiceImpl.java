package com.knappbarry.bitcoin.exchanger.service.impl;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.knappbarry.bitcoin.exchanger.clients.BlockchainInfoClient;
import com.knappbarry.bitcoin.exchanger.model.BlockchainInfoCurrency;
import com.knappbarry.bitcoin.exchanger.service.ExchangerService;
import com.knappbarry.bitcoin.exchanger.strategy.BuyHoldSellOnBookPressureStrategy;

/**
 * 1. Every 10 seconds call for open orders for each exchange BTC_DOG, BTC_LTC
 * 2. Save the open bids and asks 3. calculate the buy vs sell pressure a. buy
 * pressure add the following: -1% below lowest ask price * number -2% below
 * lowest ask price * number / 2 -3% below lowest ask price * number / 3
 * 
 * b. sell pressure 3% above highest ask -1% above highest ask price * number
 * -2% above highest ask price * number / 2 -3% above highest ask price * number
 * / 3
 * 
 * 
 * 4. If the buy pressure is > sell pressure by >3% then buy all coins in the 1%
 * range up to $1000 if the coin balance of BTC > $1000 5. if the sell pressure
 * is > buy pressure then sell all funds in the available coin balance if
 * balance is > $100 6. validate order every 10 seconds until order is processed
 * 7. if order is not processed and pressure differences switch then cancel
 * order
 * 
 * @author sdbxk01
 * 
 */
public class ExchangerServiceImpl implements ExchangerService {

	private ConfigurationsServiceImpl configs;
	private BuyHoldSellOnBookPressureStrategy buyHoldSellOnBookPressureStrategy;
	private BlockchainInfoClient blockchainInfoClient;

	Logger logger = LogManager.getLogger(ExchangerServiceImpl.class);

	@Transactional
	@Override
	public void checkForTradeOpportunities(boolean restart) throws Exception {

		Optional<BigDecimal> priceOfBitcoin = fetchBitcoinPriceInUSD();

		for (String accountKey : configs.getAccounts()) {

			try {
				buyHoldSellOnBookPressureStrategy.process(accountKey, restart, priceOfBitcoin);
			} catch (Exception e) {
				logger.error(String.format("checkForTradeOpportunities %s %s %s", accountKey, restart, priceOfBitcoin));
			}
		}
	}

	@Transactional
	@Override
	public void checkForTradeOpportunities(boolean restart, String accountKey) throws Exception {

		Optional<BigDecimal> priceOfBitcoin = fetchBitcoinPriceInUSD();

		try {
			buyHoldSellOnBookPressureStrategy.process(accountKey, restart, priceOfBitcoin);
		} catch (Exception e) {
			logger.error(String.format("checkForTradeOpportunities %s %s %s", accountKey, restart, priceOfBitcoin));
		}

	}

	private Optional<BigDecimal> fetchBitcoinPriceInUSD() throws Exception {
		try {
			Map<String, BlockchainInfoCurrency> currencies = blockchainInfoClient.getTicker();
			if (currencies != null) {
				BlockchainInfoCurrency usd = currencies.get("USD");
				if (usd != null) {
					return Optional.ofNullable(usd.getLast());
				}

			}
		} catch (Exception e) {
			logger.info("Error calling blockchaininfo, ", e);
		}

		return Optional.empty();
	}

	public void setConfigs(ConfigurationsServiceImpl configs) {
		this.configs = configs;
	}

	public void setBuyHoldSellOnBookPressureStrategy(
			BuyHoldSellOnBookPressureStrategy buyHoldSellOnBookPressureStrategy) {
		this.buyHoldSellOnBookPressureStrategy = buyHoldSellOnBookPressureStrategy;
	}

	public void setBlockchainInfoClient(BlockchainInfoClient blockchainInfoClient) {
		this.blockchainInfoClient = blockchainInfoClient;
	}

}
