package com.knappbarry.bitcoin.exchanger.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;

import com.fasterxml.jackson.annotation.JsonFormat;

@AutoProperty
@Entity
@Table(name = "BTC_BALANCE")
public class BtcBalance {

	
	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "BTC_BALANCE")
	Double btcAmount;

	@Column(name = "OWNER")
	String accountOwner;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm")
	@Column(name = "CREATE_TS")
	private Date createTs;

	@JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
	@Column(name = "USD_ACCOUNT_VALUE")
	private Double usdValue;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	

	public Double getUsdValue() {
		return usdValue;
	}

	public void setUsdValue(Double usdValue) {
		this.usdValue = usdValue;
	}

	public Double getBtcAmount() {
		return btcAmount;
	}

	public void setBtcAmount(Double btcAmount) {
		this.btcAmount = btcAmount;
	}

	public String getAccountOwner() {
		return accountOwner;
	}

	public void setAccountOwner(String accountOwner) {
		this.accountOwner = accountOwner;
	}

	public Date getCreateTs() {
		return createTs;
	}

	public void setCreateTs(Date createTs) {
		this.createTs = createTs;
	}
	
	@Override
	public int hashCode() {
		return Pojomatic.hashCode(this);
	}

	@Override
	public String toString() {
		return Pojomatic.toString(this);
	}

	@Override
	public boolean equals(Object o) {
		return Pojomatic.equals(this, o);
	}

}
