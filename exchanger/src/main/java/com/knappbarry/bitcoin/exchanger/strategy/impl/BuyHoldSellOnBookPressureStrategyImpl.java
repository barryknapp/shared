package com.knappbarry.bitcoin.exchanger.strategy.impl;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.knappbarry.bitcoin.exchanger.OrderStatusEnum;
import com.knappbarry.bitcoin.exchanger.clients.ExchangeClient;
import com.knappbarry.bitcoin.exchanger.dao.BtcBalanceDao;
import com.knappbarry.bitcoin.exchanger.dao.CompleteOrderDao;
import com.knappbarry.bitcoin.exchanger.dao.RestartDao;
import com.knappbarry.bitcoin.exchanger.model.AccountConfiguration;
import com.knappbarry.bitcoin.exchanger.model.AvailableFunds;
import com.knappbarry.bitcoin.exchanger.model.BookEntry;
import com.knappbarry.bitcoin.exchanger.model.BtcBalance;
import com.knappbarry.bitcoin.exchanger.model.CompleteOrder;
import com.knappbarry.bitcoin.exchanger.model.OrderStatus;
import com.knappbarry.bitcoin.exchanger.model.Restart;
import com.knappbarry.bitcoin.exchanger.model.Status;
import com.knappbarry.bitcoin.exchanger.model.TradeType;
import com.knappbarry.bitcoin.exchanger.service.AccountService;
import com.knappbarry.bitcoin.exchanger.service.OrderBookService;
import com.knappbarry.bitcoin.exchanger.service.OrderValidationService;
import com.knappbarry.bitcoin.exchanger.service.impl.ConfigurationsServiceImpl;
import com.knappbarry.bitcoin.exchanger.strategy.BuyHoldSellOnBookPressureStrategy;

public class BuyHoldSellOnBookPressureStrategyImpl implements BuyHoldSellOnBookPressureStrategy {

	private OrderBookService orderBookService;
	private AccountService accountService;
	private OrderValidationService orderValidationService;
	private ConfigurationsServiceImpl configs;
	private CompleteOrderDao completeOrderDao;
	private ExchangeClient exchangeClient;
	private RestartDao restartDao;
	private BtcBalanceDao btcBalanceDao;

	private int BLOCK_PURCHASE_MINUTES_AFTER_SELL_COMPLETE = 30;
	private double MIN_BTC_TO_ALWAYS_KEEP_IN_ACCOUNT = .1;
	private Map<String, Date> recentlySoldExchanges = new HashMap<String, Date>();

	Logger logger = LogManager.getLogger(BuyHoldSellOnBookPressureStrategyImpl.class);

	@Transactional
	@Override
	public synchronized void process(final String accountKey, final boolean restart, final Optional<BigDecimal> priceOfBitcoin)
			throws Exception {

				//CONFIDENTIAL MAGIC ALGORITHMS REMOVED
	}


	public void setOrderBookService(OrderBookService orderBookService) {
		this.orderBookService = orderBookService;
	}

	public void setAccountService(AccountService accountService) {
		this.accountService = accountService;
	}

	public void setOrderValidationService(OrderValidationService orderValidationService) {
		this.orderValidationService = orderValidationService;
	}

	public void setConfigs(ConfigurationsServiceImpl configs) {
		this.configs = configs;
	}

	public void setCompleteOrderDao(CompleteOrderDao completeOrderDao) {
		this.completeOrderDao = completeOrderDao;
	}

	public void setExchangeClient(ExchangeClient exchangeClient) {
		this.exchangeClient = exchangeClient;
	}

	public void setRestartDao(RestartDao restartDao) {
		this.restartDao = restartDao;
	}

	public void setBtcBalanceDao(BtcBalanceDao btcBalanceDao) {
		this.btcBalanceDao = btcBalanceDao;
	}

}
