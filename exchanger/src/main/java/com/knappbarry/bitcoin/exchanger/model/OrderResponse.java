package com.knappbarry.bitcoin.exchanger.model;

import java.util.Date;

public class OrderResponse {

	private String order_id;
	private String msg;
	private Date createTs;
	public String getOrder_id() {
		return order_id;
	}
	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Date getCreateTs() {
		return createTs;
	}
	public void setCreateTs(Date createTs) {
		this.createTs = createTs;
	}
	@Override
	public String toString() {
		return "OrderResponse [order_id=" + order_id + ", msg=" + msg
				+ ", createTs=" + createTs + "]";
	}
	
	
}

