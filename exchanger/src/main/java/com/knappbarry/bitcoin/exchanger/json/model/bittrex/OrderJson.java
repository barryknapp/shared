package com.knappbarry.bitcoin.exchanger.json.model.bittrex;

import java.util.Date;

import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;
@AutoProperty
public class OrderJson {

	private String uuid;
	private String OrderUuid;
	private String Exchange;
	private String OrderType;
	private Double Quantity;
	private Double QuantityRemaining;
	private Double Limit;
	private Double CommissionPaid;
	private Double Price;
	private Double PricePerUnit;
	private Date Opened;
	private Date Closed;
	private Boolean CancelInitiated;
	private Boolean ImmediateOrCancel;
	private Boolean isConditional;
	private String Condition;
	private String ConditionTarget;
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		uuid = uuid;
	}
	public String getOrderUuid() {
		return OrderUuid;
	}
	public void setOrderUuid(String orderUuid) {
		OrderUuid = orderUuid;
	}
	public String getExchange() {
		return Exchange;
	}
	public void setExchange(String exchange) {
		Exchange = exchange;
	}
	public String getOrderType() {
		return OrderType;
	}
	public void setOrderType(String orderType) {
		OrderType = orderType;
	}
	public Double getQuantity() {
		return Quantity;
	}
	public void setQuantity(Double quantity) {
		Quantity = quantity;
	}
	public Double getQuantityRemaining() {
		return QuantityRemaining;
	}
	public void setQuantityRemaining(Double quantityRemaining) {
		QuantityRemaining = quantityRemaining;
	}
	public Double getLimit() {
		return Limit;
	}
	public void setLimit(Double limit) {
		Limit = limit;
	}
	public Double getCommissionPaid() {
		return CommissionPaid;
	}
	public void setCommissionPaid(Double commissionPaid) {
		CommissionPaid = commissionPaid;
	}
	public Double getPrice() {
		return Price;
	}
	public void setPrice(Double price) {
		Price = price;
	}
	public Double getPricePerUnit() {
		return PricePerUnit;
	}
	public void setPricePerUnit(Double pricePerUnit) {
		PricePerUnit = pricePerUnit;
	}
	public Date getOpened() {
		return Opened;
	}
	public void setOpened(Date opened) {
		Opened = opened;
	}
	public Date getClosed() {
		return Closed;
	}
	public void setClosed(Date closed) {
		Closed = closed;
	}
	public Boolean getCancelInitiated() {
		return CancelInitiated;
	}
	public void setCancelInitiated(Boolean cancelInitiated) {
		CancelInitiated = cancelInitiated;
	}
	public Boolean getImmediateOrCancel() {
		return ImmediateOrCancel;
	}
	public void setImmediateOrCancel(Boolean immediateOrCancel) {
		ImmediateOrCancel = immediateOrCancel;
	}
	public Boolean getIsConditional() {
		return isConditional;
	}
	public void setIsConditional(Boolean isConditional) {
		this.isConditional = isConditional;
	}
	public String getCondition() {
		return Condition;
	}
	public void setCondition(String condition) {
		Condition = condition;
	}
	public String getConditionTarget() {
		return ConditionTarget;
	}
	public void setConditionTarget(String conditionTarget) {
		ConditionTarget = conditionTarget;
	}
	

	@Override
	public String toString() {
		return Pojomatic.toString(this);
	}

	@Override
	public int hashCode() {
		return Pojomatic.hashCode(this);
	}

	@Override
	public boolean equals(Object o) {
		return Pojomatic.equals(this, o);
	}	
	
}

