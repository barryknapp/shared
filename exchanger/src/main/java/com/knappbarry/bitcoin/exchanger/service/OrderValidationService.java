package com.knappbarry.bitcoin.exchanger.service;

import com.knappbarry.bitcoin.exchanger.model.AccountConfiguration;
import com.knappbarry.bitcoin.exchanger.model.AvailableFunds;

public interface OrderValidationService {

	void validateOrders(AvailableFunds availableFundsInExchange, String strategy, AccountConfiguration accountConfig)
			throws Exception;

}
