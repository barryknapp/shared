package com.knappbarry.bitcoin.exchanger.json.model.bittrex;

import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;

@AutoProperty
public class BitcoinMarketJson {

	private String MarketName;
	private Double High;
	private Double Low;
	private Double Volume;
	private Double Last;
	private Double BaseVolume;
	private String TimeStamp;
	private Double Bid;
	private Double Ask;
	private Integer OpenBuyOrders;
	private Integer OpenSellOrders;
	private Double PrevDay;
	private String Created;



	public String getMarketName() {
		return MarketName;
	}

	public void setMarketName(String marketName) {
		MarketName = marketName;
	}

	public Double getHigh() {
		return High;
	}

	public void setHigh(Double high) {
		High = high;
	}

	public Double getLow() {
		return Low;
	}

	public void setLow(Double low) {
		Low = low;
	}

	public Double getVolume() {
		return Volume;
	}

	public void setVolume(Double volume) {
		Volume = volume;
	}

	public Double getLast() {
		return Last;
	}

	public void setLast(Double last) {
		Last = last;
	}

	public Double getBaseVolume() {
		return BaseVolume;
	}

	public void setBaseVolume(Double baseVolume) {
		BaseVolume = baseVolume;
	}

	public String getTimeStamp() {
		return TimeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		TimeStamp = timeStamp;
	}

	public Double getBid() {
		return Bid;
	}

	public void setBid(Double bid) {
		Bid = bid;
	}

	public Double getAsk() {
		return Ask;
	}

	public void setAsk(Double ask) {
		Ask = ask;
	}

	public Integer getOpenBuyOrders() {
		return OpenBuyOrders;
	}

	public void setOpenBuyOrders(Integer openBuyOrders) {
		OpenBuyOrders = openBuyOrders;
	}

	public Integer getOpenSellOrders() {
		return OpenSellOrders;
	}

	public void setOpenSellOrders(Integer openSellOrders) {
		OpenSellOrders = openSellOrders;
	}

	public Double getPrevDay() {
		return PrevDay;
	}

	public void setPrevDay(Double prevDay) {
		PrevDay = prevDay;
	}

	public String getCreated() {
		return Created;
	}

	public void setCreated(String created) {
		Created = created;
	}

	@Override
	public String toString() {
		return Pojomatic.toString(this);
	}

	@Override
	public int hashCode() {
		return Pojomatic.hashCode(this);
	}

	@Override
	public boolean equals(Object o) {
		return Pojomatic.equals(this, o);
	}
}
