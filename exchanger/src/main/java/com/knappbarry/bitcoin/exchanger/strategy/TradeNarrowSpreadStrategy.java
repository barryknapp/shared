package com.knappbarry.bitcoin.exchanger.strategy;

import java.util.List;

public interface TradeNarrowSpreadStrategy {

	void process(List<String> coinExchanges) throws Exception;

}
