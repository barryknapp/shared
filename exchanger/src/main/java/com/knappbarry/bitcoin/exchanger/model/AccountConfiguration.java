package com.knappbarry.bitcoin.exchanger.model;

import java.util.List;

public class AccountConfiguration {

	private final String apiKey;
	private final String apiSecret;
	private final List<String> coinExchanges;
	private final Boolean restart;
	private final String accountKey;

	public AccountConfiguration(final String accountKey, final String apiKey, final String apiSecret,
			final List<String> coinExchanges, final Boolean restart) {
		this.apiSecret = apiSecret;
		this.apiKey = apiKey;
		this.coinExchanges = coinExchanges;
		this.restart = restart;
		this.accountKey = accountKey;	
	}

	public String getApiKey() {
		return apiKey;
	}

	public String getApiSecret() {
		return apiSecret;
	}

	public List<String> getCoinExchanges() {
		return coinExchanges;
	}

	public Boolean getRestart() {
		return restart;
	}

	public String getAccountKey() {
		return accountKey;
	}

}
