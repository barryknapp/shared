package com.knappbarry.bitcoin.exchanger.json.model.bittrex;

import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;

@AutoProperty
public class AvailableFundJsonBittrex2_0 {


	private CurrencyJson Currency;
	private BalanceJson Balance;
	private BitcoinMarketJson BitcoinMarket;

	
	

	public CurrencyJson getCurrency() {
		return Currency;
	}

	public void setCurrency(CurrencyJson currency) {
		Currency = currency;
	}

	public BalanceJson getBalance() {
		return Balance;
	}

	public void setBalance(BalanceJson balance) {
		Balance = balance;
	}

	public BitcoinMarketJson getBitcoinMarket() {
		return BitcoinMarket;
	}

	public void setBitcoinMarket(BitcoinMarketJson bitcoinMarket) {
		BitcoinMarket = bitcoinMarket;
	}

	@Override
	public String toString() {
		return Pojomatic.toString(this);
	}

	@Override
	public int hashCode() {
		return Pojomatic.hashCode(this);
	}

	@Override
	public boolean equals(Object o) {
		return Pojomatic.equals(this, o);
	}
}
