package com.knappbarry.bitcoin.exchanger.json.model.bittrex;

import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;

@AutoProperty
public class BalanceJson {

	private String Uuid;
	private Integer AccountId;
	private String Currency;
	private Double Balance;
	private Double Available;
	private Double Pending;
	private String CryptoAddress;
	private Boolean Requested;
	private String Updated;
	private Boolean AutoSell;


	public String getUuid() {
		return Uuid;
	}

	public void setUuid(String uuid) {
		Uuid = uuid;
	}

	public Integer getAccountId() {
		return AccountId;
	}

	public void setAccountId(Integer accountId) {
		AccountId = accountId;
	}

	public String getCurrency() {
		return Currency;
	}

	public void setCurrency(String currency) {
		Currency = currency;
	}

	public Double getBalance() {
		return Balance;
	}

	public void setBalance(Double balance) {
		Balance = balance;
	}

	public Double getAvailable() {
		return Available;
	}

	public void setAvailable(Double available) {
		Available = available;
	}

	public Double getPending() {
		return Pending;
	}

	public void setPending(Double pending) {
		Pending = pending;
	}

	public String getCryptoAddress() {
		return CryptoAddress;
	}

	public void setCryptoAddress(String cryptoAddress) {
		CryptoAddress = cryptoAddress;
	}

	public Boolean getRequested() {
		return Requested;
	}

	public void setRequested(Boolean requested) {
		Requested = requested;
	}

	public String getUpdated() {
		return Updated;
	}

	public void setUpdated(String updated) {
		Updated = updated;
	}

	public Boolean getAutoSell() {
		return AutoSell;
	}

	public void setAutoSell(Boolean autoSell) {
		AutoSell = autoSell;
	}

	@Override
	public String toString() {
		return Pojomatic.toString(this);
	}

	@Override
	public int hashCode() {
		return Pojomatic.hashCode(this);
	}

	@Override
	public boolean equals(Object o) {
		return Pojomatic.equals(this, o);
	}
	
}
