package com.knappbarry.bitcoin.exchanger.model;

import java.util.Arrays;
import java.util.Date;

public class OrderStatusList {

	String msg;
	OrderStatus[] orders;
	Date createTs;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public OrderStatus[] getOrders() {
		return orders;
	}

	public void setOrders(OrderStatus[] orders) {
		this.orders = orders;
	}

	public Date getCreateTs() {
		return createTs;
	}

	public void setCreateTs(Date createTs) {
		this.createTs = createTs;
	}

	@Override
	public String toString() {
		return "OrderStatusList [msg=" + msg + ", orders="
				+ Arrays.toString(orders) + ", createTs=" + createTs + "]";
	}
	

}