package com.knappbarry.bitcoin.exchanger.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.knappbarry.bitcoin.exchanger.clients.ExchangeClient;
import com.knappbarry.bitcoin.exchanger.dao.CompleteOrderDao;
import com.knappbarry.bitcoin.exchanger.model.AccountConfiguration;
import com.knappbarry.bitcoin.exchanger.model.AvailableFunds;
import com.knappbarry.bitcoin.exchanger.model.BookEntry;
import com.knappbarry.bitcoin.exchanger.model.CoinBalance;
import com.knappbarry.bitcoin.exchanger.model.CompleteOrder;
import com.knappbarry.bitcoin.exchanger.model.OrderResponse;
import com.knappbarry.bitcoin.exchanger.model.Status;
import com.knappbarry.bitcoin.exchanger.service.AccountService;
import com.knappbarry.bitcoin.exchanger.service.OrderBookService;

public class AccountServiceImpl implements AccountService {

	ExchangeClient exchangeClient;
	CompleteOrderDao completeOrderDao;
	ConfigurationsServiceImpl configs;
	OrderBookService orderBookService;

	Logger logger = LogManager.getLogger(AccountServiceImpl.class);

	final Double MAX_LEVERAGE = .1;// 10% in any single coin

	@Override
	public Double getAvailableFundsForCoin(AvailableFunds availableFunds, String coinType) throws Exception {
		for (CoinBalance balance : availableFunds.getCoinBalances()) {
			if (balance.getCoinSymbol().equalsIgnoreCase(coinType)) {
				return balance.getBalance();
			}
		}
		return new Double(0);
	}

	/**
	 * Parameter: Parameter Description Example pair currency pair ltc_btc type
	 * trading type SELL or BUY rate The rate to buy or sell 0.023 amount The
	 * amount to buy or sell 100
	 */
	@Override
	public CompleteOrder performBuy(Double coinsToTrade, String exchange, Double price, String strategy, final AccountConfiguration accountConfig)
			throws Exception {
		Map<String, String> params = new HashMap<String, String>();

		if (coinsToTrade.equals(new Double(0))) {
			return null;
		}

		params.put("pair", exchange);
		params.put("type", "BUY");

		params.put("rate", String.valueOf(price));
		params.put("amount", String.valueOf(coinsToTrade));

		OrderResponse orderResponse = exchangeClient.placeOrder(params, strategy, accountConfig);

		if (orderResponse==null || orderResponse.getOrder_id() == null || orderResponse.getOrder_id().equals("0")) {
			return null;
		}

		CompleteOrder order = new CompleteOrder();
		order.setAccountKey(accountConfig.getAccountKey());
		order.setAmount(coinsToTrade);
		order.setAmountToClose(coinsToTrade);
		order.setPrice(price);
		order.setCreateTs(new Date());
		order.setOrderId(orderResponse.getOrder_id());
		order.setExchange(exchange);
		order.setStrategy(strategy);
		order.setStatus(Status.BUY_SUBMIT);
		order.setWebsite(exchangeClient.getWebsiteName());

		logger.debug(accountConfig.getAccountKey() + " BUY SUBMITTED: " + order);

		completeOrderDao.save(order);

		return order;

	}

	// a sell order should never insert a new row, it is used only to close
	// existing buy orders
	@Override
	public CompleteOrder performSell(String exchange, Double price, String strategy, CompleteOrder orderToClose, final AccountConfiguration accountConfig)
			throws Exception {
		Map<String, String> params = new HashMap<String, String>();

		params.put("pair", exchange);
		params.put("type", "SELL");

		params.put("rate", String.valueOf(price));
		params.put("amount", String.valueOf(orderToClose.getAmountToClose()));

		if (orderToClose.getPrice() > price * (1 - configs.minPercentRangeToTrade())) {

			logger.debug("Cancel sell order price " + price + " is less than original purchase price " + orderToClose);
			return null;
		}

		OrderResponse orderResponse = exchangeClient.placeOrder(params, strategy, accountConfig);

		if(orderResponse==null){
			logger.error("Sell order completely failed to process by exchange!! Is the amount = 0?" + params);
			return null;
		}
		
		if (orderResponse.getOrder_id() == null || orderResponse.getOrder_id().equals("0")) {
			return null;
		}
		orderToClose.setSellOrderId(orderResponse.getOrder_id());
		orderToClose.setUpdateTs(new Date());
		orderToClose.setStatus(Status.SELL_SUBMIT);

		completeOrderDao.save(orderToClose);

		logger.debug(accountConfig.getAccountKey() + " SELL SUBMITTED " + price + " Order:" + orderToClose + " Exchange:" + exchange);

		return orderToClose;

	}

	public void setCompleteOrderDao(CompleteOrderDao completeOrderDao) {
		this.completeOrderDao = completeOrderDao;
	}

	public void setConfigs(ConfigurationsServiceImpl configs) {
		this.configs = configs;
	}

	/**
	 * remove echanges that are over leveraged
	 * 
	 * @param exchanges
	 * @return
	 */
	@Override
	public List<String> filterOutOverBoughtExchanges(List<String> exchanges, String strategy, final AccountConfiguration accountConfig) throws Exception {
		// should not have more than 20% funds in any one currency
		List<String> filteredExchanges = new ArrayList<String>();

		Map<String, Double> btcValuePerOwnedCoin = calculateBtcOwnedPerCoin(exchanges, strategy, accountConfig);

		Double totalBtcInvested = getTotalValue(btcValuePerOwnedCoin.values());
		if (totalBtcInvested == null || totalBtcInvested == 0) {
			return exchanges;
		}

		for (String exchange : exchanges) {
			Double btcValueOfAlreadyOwnedCoins = btcValuePerOwnedCoin.get(exchange);

			if (btcValueOfAlreadyOwnedCoins == null || btcValueOfAlreadyOwnedCoins / totalBtcInvested < MAX_LEVERAGE) {
				filteredExchanges.add(exchange);
			}
		}

		return filteredExchanges;

	}

	@Override
	public Map<String, Double> calculateBtcOwnedPerCoin(List<String> exchanges, String strategy, final AccountConfiguration accountConfig) throws Exception {
		Map<String, Double> btcValuePerOwnedOrPendingCoin = new HashMap<String, Double>();

		for (String exchange : exchanges) {
			Double altCoinsAlreadyOwned = completeOrderDao.findTotalAmountOpenOrdersForExchangeAndWebsiteAndAccountKey(exchange, exchangeClient.getWebsiteName(), accountConfig.getAccountKey());
			Double btcValueOfAlreadyOwnedOrPendingCoins;
			if (altCoinsAlreadyOwned == null) {
				btcValueOfAlreadyOwnedOrPendingCoins = new Double(0);
			} else {
				BookEntry orderBook = exchangeClient.getOrderBook(exchange, strategy, accountConfig);
				String coinType = configs.getCoinForExchange(exchange);
				btcValueOfAlreadyOwnedOrPendingCoins = orderBookService.convertBalanceToBtcWithMinTradeOffset(altCoinsAlreadyOwned, coinType,
						orderBook);
			}
			btcValuePerOwnedOrPendingCoin.put(exchange, btcValueOfAlreadyOwnedOrPendingCoins);

		}
		return btcValuePerOwnedOrPendingCoin;
	}

	private Double getTotalValue(Collection<Double> values) {
		Double total = new Double(0);
		for (Double value : values) {
			if (value != null) {
				total += value;
			}
		}
		return total;
	}

	/**
	 * Try to close the order with the lowest paid price first
	 * 
	 * @param exchange
	 * @param strategy
	 * @return
	 */
	@Override
	public CompleteOrder getOrderWithLowestPaidPrice(String exchange, String strategy, final AccountConfiguration accountConfig) {
		List<CompleteOrder> orders = completeOrderDao.findByStatusAndExchangeAndStrategyAndWebsiteAndAccountKey(Status.BUY_CLOSED, exchange,
				strategy, exchangeClient.getWebsiteName(), accountConfig.getAccountKey());
		CompleteOrder orderWithLowestPrice = null;
		for (CompleteOrder order : orders) {
			if (orderWithLowestPrice == null && order.getPriceConfirmed() != null) {
				orderWithLowestPrice = order;
			}
			if (order.getPriceConfirmed() != null
					&& orderWithLowestPrice.getPriceConfirmed() > order.getPriceConfirmed()) {
				orderWithLowestPrice = order;
			}
		}
		return orderWithLowestPrice;
	}

	@Override
	public CompleteOrder getOrderWithHighestPaidPrice(String exchange, String strategy, final AccountConfiguration accountConfig) {
		List<CompleteOrder> orders = completeOrderDao.findByStatusAndExchangeAndStrategyAndWebsiteAndAccountKey(Status.BUY_CLOSED, exchange,
				strategy, exchangeClient.getWebsiteName(), accountConfig.getAccountKey());
		CompleteOrder orderWithHighestPrice = null;
		for (CompleteOrder order : orders) {
			if (orderWithHighestPrice == null && order.getPriceConfirmed() != null) {
				orderWithHighestPrice = order;
			}
			if (order.getPriceConfirmed() != null
					&& orderWithHighestPrice.getPriceConfirmed() < order.getPriceConfirmed()) {
				orderWithHighestPrice = order;
			}
		}
		return orderWithHighestPrice;
	}


	public void setOrderBookService(OrderBookService orderBookService) {
		this.orderBookService = orderBookService;
	}

	public void setExchangeClient(ExchangeClient exchangeClient) {
		this.exchangeClient = exchangeClient;
	}
}
