package com.knappbarry.bitcoin.exchanger;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.knappbarry.bitcoin.exchanger.dao.CompleteOrderDao;
import com.knappbarry.bitcoin.exchanger.model.CompleteOrder;
import com.knappbarry.bitcoin.exchanger.service.ExchangerService;

public class ApplicationSpring {

	static int five_minutes = 1000 * 60 * 5;
	static int thirty_minutes = 1000 * 60 * 30;

	static Logger logger = LogManager.getLogger(ApplicationSpring.class);
	static Boolean restart = false;

	public static void main(String[] args) throws Exception {

		ConfigurableApplicationContext context = SpringApplication.run(Config.class);
		ExchangerService serviceBittrex = (ExchangerService) context.getBean("exchangerServiceBittrex");

		CompleteOrderDao dao = (CompleteOrderDao) context.getBean("completeOrderDao");
		if (restart) {
			logger.error("RESTARTING!!!!!!!! ");
			Thread.sleep(10000);
			dao.deleteAll();
		}

		while (true) {
			try {
				serviceBittrex.checkForTradeOpportunities(restart);
			} catch (Exception e) {
				logger.error("Error ", e);
			}

			try {
				// serviceBter.checkForTradeOpportunities(restart);
			} catch (Exception e) {
				logger.error("Error ", e);
			}

			if (restart) {
				// delete 0's
				for (CompleteOrder o : dao.findAll()) {
					if (o.getAmount() == 0) {
						dao.delete(o);
					}
				}
				System.exit(0);
			}

			Thread.sleep(getSleepTime());
		}
	}

	private static long getSleepTime() {
		return five_minutes;
	}

}
