package com.knappbarry.bitcoin.exchanger.json.model.bter;

import java.util.Arrays;




public class BookEntryJson {
	
	boolean result;
	
	private String[][] asks;
	private String[][] bids;
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public String[][] getAsks() {
		return asks;
	}
	public void setAsks(String[][] asks) {
		this.asks = asks;
	}
	public String[][] getBids() {
		return bids;
	}
	public void setBids(String[][] bids) {
		this.bids = bids;
	}
	@Override
	public String toString() {
		return "BookEntryJson [result=" + result + ", asks="
				+ Arrays.toString(asks) + ", bids=" + Arrays.toString(bids)
				+ "]";
	}

	

	
}
