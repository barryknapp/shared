package com.knappbarry.bitcoin.exchanger.dao;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.knappbarry.bitcoin.exchanger.model.Restart;

public interface RestartDao  extends CrudRepository<Restart, String> {

	@Query("select max(createTs) from Restart where accountKey = ?1")
	public Date findLastRestartDate(String owner);

}
