package com.knappbarry.bitcoin.exchanger.json.model.bittrex;

import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;

@AutoProperty
public class CurrencyJson {

	private String Currency;
	private String CurrencyLong;
	private Integer MinConfirmation;
	private Double TxFee;
	private Boolean IsActive;
	private String CoinType;
	private String BaseAddress;
	private String Notice;
	public String getCurrency() {
		return Currency;
	}
	public void setCurrency(String currency) {
		Currency = currency;
	}
	public String getCurrencyLong() {
		return CurrencyLong;
	}
	public void setCurrencyLong(String currencyLong) {
		CurrencyLong = currencyLong;
	}
	public Integer getMinConfirmation() {
		return MinConfirmation;
	}
	public void setMinConfirmation(Integer minConfirmation) {
		MinConfirmation = minConfirmation;
	}
	public Double getTxFee() {
		return TxFee;
	}
	public void setTxFee(Double txFee) {
		TxFee = txFee;
	}
	public Boolean getIsActive() {
		return IsActive;
	}
	public void setIsActive(Boolean isActive) {
		IsActive = isActive;
	}
	public String getCoinType() {
		return CoinType;
	}
	public void setCoinType(String coinType) {
		CoinType = coinType;
	}
	public String getBaseAddress() {
		return BaseAddress;
	}
	public void setBaseAddress(String baseAddress) {
		BaseAddress = baseAddress;
	}
	public String getNotice() {
		return Notice;
	}
	public void setNotice(String notice) {
		Notice = notice;
	}
	
	@Override
	public String toString() {
		return Pojomatic.toString(this);
	}

	@Override
	public int hashCode() {
		return Pojomatic.hashCode(this);
	}

	@Override
	public boolean equals(Object o) {
		return Pojomatic.equals(this, o);
	}
}
