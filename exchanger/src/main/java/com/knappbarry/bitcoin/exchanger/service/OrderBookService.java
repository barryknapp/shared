package com.knappbarry.bitcoin.exchanger.service;

import com.knappbarry.bitcoin.exchanger.model.BookEntry;
import com.knappbarry.bitcoin.exchanger.model.TradeType;

public interface OrderBookService {

	Double calculateBuyPressure(BookEntry orderBook) throws Exception;

	Double calculateSellPressure(BookEntry orderBook) throws Exception;

	Double calculateAmountWithinPercentRange(BookEntry orderBook, Double percent, TradeType tradeType ) throws Exception;

	Double convertBalanceToAltCoin(Double btcBalance, String coinType, Double price);

	Double convertBalanceToBtcWithMinTradeOffset(Double altCoinBalance, String coinType, BookEntry orderBook);

	Double getHighestBuyPrice(BookEntry orderBook);

	Double getLowestAskPrice(BookEntry orderBook);

	Double convertBalanceToBtcBasedOnMaxBuyOnBook(Double altCoinsAlreadyOwned, String coinType, BookEntry orderBook);

}
