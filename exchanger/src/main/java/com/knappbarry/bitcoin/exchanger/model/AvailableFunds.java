package com.knappbarry.bitcoin.exchanger.model;

import java.util.Date;
import java.util.List;

public class AvailableFunds {

	private Date createTs;
	private List<CoinBalance> coinBalances;
	public Date getCreateTs() {
		return createTs;
	}
	public void setCreateTs(Date createTs) {
		this.createTs = createTs;
	}
	public List<CoinBalance> getCoinBalances() {
		return coinBalances;
	}
	public void setCoinBalances(List<CoinBalance> coinBalances) {
		this.coinBalances = coinBalances;
	}
	@Override
	public String toString() {
		return "AvailableFunds [createTs=" + createTs + ", coinBalances="
				+ coinBalances + "]";
	}
	
	
}
