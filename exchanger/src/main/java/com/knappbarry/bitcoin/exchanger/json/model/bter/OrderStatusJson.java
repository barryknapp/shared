package com.knappbarry.bitcoin.exchanger.json.model.bter;

public class OrderStatusJson {

	boolean result;
	String msg;
	OrderJson order;
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public OrderJson getOrder() {
		return order;
	}
	public void setOrder(OrderJson order) {
		this.order = order;
	}
	@Override
	public String toString() {
		return "OrderStatusJson [result=" + result + ", msg=" + msg
				+ ", order=" + order + "]";
	}
	
	
}
