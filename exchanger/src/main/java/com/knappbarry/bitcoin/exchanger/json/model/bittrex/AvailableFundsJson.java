package com.knappbarry.bitcoin.exchanger.json.model.bittrex;

import java.util.List;

import org.pojomatic.Pojomatic;

public class AvailableFundsJson {

	boolean success;
	String message;
	List<AvailableFundJsonBittrex2_0> result;
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<AvailableFundJsonBittrex2_0> getResult() {
		return result;
	}
	public void setResult(List<AvailableFundJsonBittrex2_0> result) {
		this.result = result;
	}

	

	@Override
	public String toString() {
		return Pojomatic.toString(this);
	}

	@Override
	public int hashCode() {
		return Pojomatic.hashCode(this);
	}

	@Override
	public boolean equals(Object o) {
		return Pojomatic.equals(this, o);
	}


	
	
}
