package com.knappbarry.bitcoin.exchanger.json;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.knappbarry.bitcoin.exchanger.OrderStatusEnum;
import com.knappbarry.bitcoin.exchanger.json.model.bittrex.AvailableFundJsonBittrex2_0;
import com.knappbarry.bitcoin.exchanger.json.model.bittrex.BookEntryJson;
import com.knappbarry.bitcoin.exchanger.json.model.bittrex.OpenOrderJson;
import com.knappbarry.bitcoin.exchanger.json.model.bittrex.OrderJson;
import com.knappbarry.bitcoin.exchanger.json.model.bittrex.OrderStatusJson;
import com.knappbarry.bitcoin.exchanger.model.AvailableFunds;
import com.knappbarry.bitcoin.exchanger.model.BookEntry;
import com.knappbarry.bitcoin.exchanger.model.CoinBalance;
import com.knappbarry.bitcoin.exchanger.model.OpenOrder;
import com.knappbarry.bitcoin.exchanger.model.OrderResponse;
import com.knappbarry.bitcoin.exchanger.model.OrderStatus;
import com.knappbarry.bitcoin.exchanger.model.OrderStatusList;
import com.knappbarry.bitcoin.exchanger.model.TradeType;

public class BittrexJsonToObjectConverter {

	Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:SSS").create();
	Logger logger = LogManager.getLogger(BittrexJsonToObjectConverter.class);

	public AvailableFunds deserializeAvailableFunds(String json) {
		List<AvailableFundJsonBittrex2_0> avFunds = gson.fromJson(json,
				new TypeToken<List<AvailableFundJsonBittrex2_0>>() {
				}.getType());
		return convert(avFunds);

	}

	private AvailableFunds convert(List<AvailableFundJsonBittrex2_0> avFunds) {
		AvailableFunds funds = new AvailableFunds();
		funds.setCreateTs(new Date());
		funds.setCoinBalances(getCoinBalances(avFunds));
		return funds;
	}

	private List<CoinBalance> getCoinBalances(List<AvailableFundJsonBittrex2_0> avFunds) {

		List<CoinBalance> balances = new ArrayList<CoinBalance>();

		for (AvailableFundJsonBittrex2_0 avFund : avFunds) {
			if (avFund.getBalance().getAvailable() == 0) {
				continue;
			}
			CoinBalance bal = new CoinBalance();
			bal.setBalance(avFund.getBalance().getAvailable());
			bal.setCoinSymbol(avFund.getCurrency().getCurrency());
			balances.add(bal);

		}
		return balances;

	}

	public BookEntry deserializeBookEntry(String json) {

		BookEntryJson entryJson = gson.fromJson(json, BookEntryJson.class);
		return convert(entryJson);

	}

	private BookEntry convert(BookEntryJson entryJson) {
		if(entryJson==null){
			return null;
		}
		BookEntry entry = new BookEntry();
		entry.setCreateTs(new Date());
		entry.setBids(getBids(entryJson.getBuy()));
		entry.setAsks(getBids(entryJson.getSell()));
		return entry;
	}

	private List<OpenOrder> getBids(List<OpenOrderJson> openOrders) {
		List<OpenOrder> orders = new ArrayList<OpenOrder>();
		if(openOrders==null){
			return orders;
		}
		for (OpenOrderJson openOrder : openOrders) {

			OpenOrder order = new OpenOrder();
			order.setPrice(openOrder.getRate());
			order.setAmount(openOrder.getQuantity());
			orders.add(order);
		}
		return orders;

	}

	public OrderResponse deserializeOrderResponse(String json) {
		if(json==null){
			return null;
		}
		
		try {
			OrderJson resp = gson.fromJson(json, OrderJson.class);
			if(resp==null){
				logger.error("Error deserializing OrderJson " + json);
				return null;
			}
			OrderResponse order = new OrderResponse();
			order.setCreateTs(new Date());
			order.setOrder_id(resp.getUuid());
			return order;
		} catch (Exception e) {
			logger.error("Error deserializing OrderJson " + json, e);
		}

		return null;
	}

	public OrderStatusList deserializeOrderStatusList(String json) {
		OrderStatusList orderStatusList = gson.fromJson(json, OrderStatusList.class);
		orderStatusList.setCreateTs(new Date());
		return orderStatusList;
	}

	public OrderStatusJson deserializeOrderStatus(String json) {
		try {
			OrderStatusJson orderStatus = gson.fromJson(json, OrderStatusJson.class);
			return orderStatus;
		} catch (Exception e) {
			logger.error("Could not deserialize " + json, e);
			throw new RuntimeException(e);
		}
	}

	public OrderJson deserializeSubmittedOrder(String json) {
		try {
			OrderJson order = gson.fromJson(json, OrderJson.class);
			return order;
		} catch (Exception e) {
			logger.error("Could not deserialize " + json, e);
			throw new RuntimeException(e);
		}

	}

	// returns closed buy orders only
	public List<OrderStatus> deserializeOrderHistory(String json) {

		List<OrderJson> completeOrders = gson.fromJson(json, new TypeToken<List<OrderJson>>() {
		}.getType());
		return convertBuyOrders(completeOrders);
	}

	private List<OrderStatus> convertBuyOrders(List<OrderJson> completeOrders) {
		List<OrderStatus> stati = new ArrayList<OrderStatus>();
		for (OrderJson js : completeOrders) {
			OrderStatus s = new OrderStatus();
			s.setInitial_rate(js.getPricePerUnit());
			s.setStatus(OrderStatusEnum.closed);
			s.setCoin(js.getExchange().split("-")[1]);
			s.setType(js.getOrderType().equals("LIMIT_BUY") ? TradeType.buy : TradeType.sell);
			s.setId(js.getOrderUuid());
			s.setAmount(js.getQuantity());
			stati.add(s);
		}
		return stati;
	}
}
