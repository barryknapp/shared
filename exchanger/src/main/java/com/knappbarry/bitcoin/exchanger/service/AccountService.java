package com.knappbarry.bitcoin.exchanger.service;

import java.util.List;
import java.util.Map;

import com.knappbarry.bitcoin.exchanger.model.AccountConfiguration;
import com.knappbarry.bitcoin.exchanger.model.AvailableFunds;
import com.knappbarry.bitcoin.exchanger.model.CompleteOrder;

public interface AccountService {

	Double getAvailableFundsForCoin(AvailableFunds availableFunds, String coinType) throws Exception;

	CompleteOrder performBuy(Double coinsToTrade, String exchange, Double price, String strategy,
			AccountConfiguration accountConfig) throws Exception;

	CompleteOrder performSell(String exchange, Double price, String strategy, CompleteOrder orderToClose,
			AccountConfiguration accountConfig) throws Exception;

	Map<String, Double> calculateBtcOwnedPerCoin(List<String> exchanges, String strategy,
			AccountConfiguration accountConfig) throws Exception;

	List<String> filterOutOverBoughtExchanges(List<String> exchanges, String strategy,
			AccountConfiguration accountConfig) throws Exception;

	CompleteOrder getOrderWithLowestPaidPrice(String exchange, String strategy, AccountConfiguration accountConfig);

	CompleteOrder getOrderWithHighestPaidPrice(String exchange, String strategy, AccountConfiguration accountConfig);

}
