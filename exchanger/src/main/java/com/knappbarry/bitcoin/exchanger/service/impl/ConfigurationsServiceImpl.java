package com.knappbarry.bitcoin.exchanger.service.impl;

import java.util.Arrays;
import java.util.List;

import com.knappbarry.bitcoin.exchanger.dao.ConfigurationsDao;

public class ConfigurationsServiceImpl {
	
	ConfigurationsDao configurationsDao;

	public final String BTC_SYMBOL = "btc";

	
	public List<String> getCoinExchanges(final String accountKey) {
		String exchanges = configurationsDao.findByKey("bittrex.exchanges." + accountKey).get(0).getValue();		
		List<String> exs =  Arrays.asList(exchanges.split(","));
		return exs;
	}
	

	public String getCoinForExchange(String exchange) {
		return exchange.replace("_btc", "");
	}
	
	public String getExchangeForCoin(String coin){
		return coin.toLowerCase() + "_btc";
	}

	public String getBaseApiUrl() {
		return configurationsDao.findByKey("bter.api.url").get(0).getValue();
	}

	public Double getPressureDifferenceToTriggerTrade() {
		return new Double(configurationsDao.findByKey("pressure.diff.to.trigger.trade").get(0).getValue());
	}

	public String getKey(String strategy) {
		return configurationsDao.findByKey("bter.key."+strategy).get(0).getValue();
	}

	public String getSecret(String strategy) {
		return configurationsDao.findByKey("bter.secret."+strategy).get(0).getValue();
	}

	public Double getTierPercentage(int tier) {

		return new Double(configurationsDao.findByKey("tier.percent."+tier).get(0).getValue());
	}

	public int getTierWeightedDivisor(int tier) {
		return new Integer(configurationsDao.findByKey("tier.weighted.divisor."+tier).get(0).getValue());
	}

	public int getNumberOfTiersToEvaluate() {
		return new Integer(configurationsDao.findByKey("num.tiers.to.evaluate").get(0).getValue());
	}

	/**
	 * when executing a trade, this is the minimum percent within range of the
	 * buy or sell price if we want to turn a 2% profit, we should trade > 2%
	 * ranges
	 * 
	 * @return
	 */
	public Double minPercentRangeToTrade() {
		return new Double(configurationsDao.findByKey("min.percent.range.to.trade").get(0).getValue());
	}

	public Double getMaxBtcBuyAmount() {
		return new Double(configurationsDao.findByKey("max.btc.amount").get(0).getValue());
	}

	public Double minimumTradeBtcValue() {
		return new Double(configurationsDao.findByKey("min.btc.trade.value").get(0).getValue());
	}

	public void setConfigurationsDao(ConfigurationsDao configurationsDao) {
		this.configurationsDao = configurationsDao;
	}

	
	public String getBittrexApiKey(final String accountKey){
		return configurationsDao.findByKey("bittrex.api.key." + accountKey).get(0).getValue();
	}
	public String getBittrexApiSecret(final String accountKey){
		return configurationsDao.findByKey("bittrex.api.secret." + accountKey).get(0).getValue();
	}


	public List<String> getAccounts() {
		String accounts = configurationsDao.findByKey("bittrex.active.accounts").get(0).getValue();		
		List<String> acts =  Arrays.asList(accounts.split(","));
		return acts;
	}

}
