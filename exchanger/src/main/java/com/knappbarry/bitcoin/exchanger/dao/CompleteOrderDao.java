package com.knappbarry.bitcoin.exchanger.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.knappbarry.bitcoin.exchanger.model.CompleteOrder;
import com.knappbarry.bitcoin.exchanger.model.Status;

public interface CompleteOrderDao extends CrudRepository<CompleteOrder, Integer> {
	

	public List<CompleteOrder> findByStatusAndWebsiteAndAccountKey(Status status, String website, String accountKey);
	
	@Query("select sum(amountToClose) from CompleteOrder where status in ('BUY_CLOSED', 'BUY_SUBMIT','SELL_SUBMIT','SELL_CANCEL') and exchange = ?1 and website = ?2 and accountKey = ?3")
	public Double findTotalAmountOpenOrdersForExchangeAndWebsiteAndAccountKey(String exchange, String website, String accountKey);

	public List<CompleteOrder> findByStatusAndStrategyAndWebsiteAndAccountKey(String status, String strategy, String website, String accountKey);


	public List<CompleteOrder> findByStatusInAndWebsiteAndAccountKey(List<Status> asList, String website, String accountKey);


	public List<CompleteOrder> findByStatusAndExchangeAndStrategyAndWebsiteAndAccountKey(Status status, String exchange, String strategy, String website, String accountKey);


	public List<CompleteOrder> findByStatusInAndWebsiteAndExchangeAndAccountKey(List<Status> asList, String websiteName,
			String exchangeForCoin, String accountKey);


	public CompleteOrder findByOrderId(String id);


	public List<CompleteOrder> findByStatusAndWebsiteAndAccountKeyAndConfirmedWithExchangeIsNull(Status buyClosed, String websiteName, String accountKey);

	public List<CompleteOrder> findByExchangeAndStatus(String coinExchange, Status buySubmit);

	public List<CompleteOrder> findByAccountKey(String accountKey);

	 
}
