package com.knappbarry.bitcoin.exchanger.model;

public enum Status {

	BUY_SUBMIT,
	BUY_CLOSED,
	SELL_SUBMIT,
	SELL_CLOSED,
	UNKNOWN, 
	MISSING_BALANCE,
	BUY_CANCEL,
	SELL_CANCEL;

}
