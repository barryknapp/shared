package com.knappbarry.bitcoin.exchanger.json;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.knappbarry.bitcoin.exchanger.json.model.bter.AvailableFundsJson;
import com.knappbarry.bitcoin.exchanger.json.model.bter.BookEntryJson;
import com.knappbarry.bitcoin.exchanger.json.model.bter.OrderStatusJson;
import com.knappbarry.bitcoin.exchanger.model.AvailableFunds;
import com.knappbarry.bitcoin.exchanger.model.BookEntry;
import com.knappbarry.bitcoin.exchanger.model.CoinBalance;
import com.knappbarry.bitcoin.exchanger.model.OpenOrder;
import com.knappbarry.bitcoin.exchanger.model.OrderResponse;
import com.knappbarry.bitcoin.exchanger.model.OrderStatusList;

public class BterJsonToObjectConverter {

	Gson gson = new Gson();
	Logger logger = LogManager.getLogger(BterJsonToObjectConverter.class);

	public AvailableFunds deserializeAvailableFunds(String json) {
		AvailableFundsJson fundsJson = gson.fromJson(json, AvailableFundsJson.class);

		return convert(fundsJson);

	}

	private AvailableFunds convert(AvailableFundsJson fundsJson) {
		AvailableFunds funds = new AvailableFunds();
		funds.setCreateTs(new Date());
		funds.setCoinBalances(getCoinBalances(fundsJson.getAvailable_funds()));
		return funds;
	}

	private List<CoinBalance> getCoinBalances(Map<String, Double> funds) {

		List<CoinBalance> balances = new ArrayList<CoinBalance>();

		for (String symbol : funds.keySet()) {

			CoinBalance bal = new CoinBalance();
			bal.setBalance(funds.get(symbol));
			bal.setCoinSymbol(symbol);
			balances.add(bal);

		}
		return balances;

	}


	public BookEntry deserializeBookEntry(String json) {

		BookEntryJson entryJson = gson.fromJson(json, BookEntryJson.class);
		return convert(entryJson);

	}

	private BookEntry convert(BookEntryJson entryJson) {
		if(!entryJson.isResult()){
			return null;
		}
		BookEntry entry = new BookEntry();
		entry.setCreateTs(new Date());
		entry.setBids(getBids(entryJson.getBids()));
		entry.setAsks(getBids(entryJson.getAsks()));
		return entry;
	}

	private List<OpenOrder> getBids(String[][] open) {
		List<OpenOrder> orders = new ArrayList<OpenOrder>();
		int i = 0;
		while (i < open.length) {
			String price = open[i][0];
			String amount = open[i][1];
			OpenOrder order = new OpenOrder();
			order.setPrice(Double.parseDouble(price));
			order.setAmount(Double.parseDouble(amount));

			orders.add(order);
			i++;

		}
		return orders;

	}

	public OrderResponse deserializeOrderResponse(String json) {
		OrderResponse resp = gson.fromJson(json, OrderResponse.class);
		resp.setCreateTs(new Date());
		return resp;
	}

	public OrderStatusList deserializeOrderStatusList(String json) {
		OrderStatusList orderStatusList = gson.fromJson(json, OrderStatusList.class);
		orderStatusList.setCreateTs(new Date());
		return orderStatusList;
	}

	public OrderStatusJson deserializeOrderStatus(String json) {
		try {
			OrderStatusJson orderStatus = gson.fromJson(json, OrderStatusJson.class);
			return orderStatus;
		} catch (Exception e) {
			logger.error("Could not deserialize " + json, e);
			throw new RuntimeException(e);
		}
	}
}
