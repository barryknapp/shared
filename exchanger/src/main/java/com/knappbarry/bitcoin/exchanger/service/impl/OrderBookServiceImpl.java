package com.knappbarry.bitcoin.exchanger.service.impl;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.knappbarry.bitcoin.exchanger.model.BookEntry;
import com.knappbarry.bitcoin.exchanger.model.OpenOrder;
import com.knappbarry.bitcoin.exchanger.model.TradeType;
import com.knappbarry.bitcoin.exchanger.service.OrderBookService;

public class OrderBookServiceImpl implements OrderBookService {
	
	ConfigurationsServiceImpl configs;
	Logger logger = LogManager.getLogger(OrderBookServiceImpl.class);


	@Override
	public Double calculateSellPressure(BookEntry orderBook) throws Exception {

		int tier = configs.getNumberOfTiersToEvaluate();
		Double pressure = Double.MIN_VALUE;
		while (tier > 0) {
			//a buy trade is interested in the sell pressure
			pressure += calculateAmountWithinPercentRange(orderBook,
					configs.getTierPercentage(tier), TradeType.buy)
					/ configs.getTierWeightedDivisor(tier);
			tier--;
		}

		return pressure;
	}

	@Override
	public Double getHighestBuyPrice(BookEntry orderBook) {
		Double highestBuy = Double.MIN_VALUE;
		for (OpenOrder order : orderBook.getBids()) {
			if (highestBuy < order.getPrice()) {
				highestBuy = order.getPrice();
			}
		}

		return highestBuy;
	}

	@Override
	public Double calculateBuyPressure(BookEntry orderBook) throws Exception {

		int tier = configs.getNumberOfTiersToEvaluate();
		Double pressure = Double.MIN_VALUE;
		while (tier > 0) {
			//a sell trade is interested in the buy pressure
			pressure += calculateAmountWithinPercentRange(orderBook,
					configs.getTierPercentage(tier), TradeType.sell)
					/ configs.getTierWeightedDivisor(tier);
			tier--;
		}

		return pressure;
	}

	@Override
	public Double getLowestAskPrice(BookEntry orderBook) {
		Double lowestSell = Double.MAX_VALUE;
		for (OpenOrder order : orderBook.getAsks()) {
			if (lowestSell > order.getPrice()) {
				lowestSell = order.getPrice();
			}
		}

		return lowestSell;
	}

	/**
	 * If you want to sell, we need to calculate the number of bids (buyers)
 	 * If you want to buy, we calculate the number of asks (sellers)
	 */
	@Override
	public Double calculateAmountWithinPercentRange(BookEntry orderBook,
			Double percent, TradeType tradeType) throws Exception {
		if (TradeType.sell == tradeType) {
			Double highestBuyPrice = getHighestBuyPrice(orderBook);
			return getAmountsGreater(highestBuyPrice * (1 - percent),
					orderBook.getBids());

		} else {
			Double lowestSellPrice = getLowestAskPrice(orderBook);
			return getAmountsLess(lowestSellPrice * (1 + percent),
					orderBook.getAsks());
		}
	}

	private Double getAmountsLess(Double price, List<OpenOrder> orders) {
		Double total = new Double(0);
		for (OpenOrder order : orders) {
			if (order.getPrice() < price) {
				total += order.getAmount();
			}
		}
		return total;
	}

	private Double getAmountsGreater(Double price, List<OpenOrder> orders) {
		Double total = new Double(0);
		for (OpenOrder order : orders) {
			if (order.getPrice() > price) {
				total += order.getAmount();
			}
		}
		return total;
	}




	/**
	 * this represents the value in BTC of the alt coin with an offset more the min trade
	 */
	@Override
	public Double convertBalanceToBtcWithMinTradeOffset(Double altCoinBalance, String coinType,
			BookEntry orderBook) {
		return convertBalanceToBtcBasedOnMaxBuyOnBook(altCoinBalance, coinType, orderBook)
				* (1 - configs.minPercentRangeToTrade());
	}
	
	@Override
	public Double convertBalanceToBtcBasedOnMaxBuyOnBook(Double altCoinBalance, String coinType,
			BookEntry orderBook) {
		Double highestBuy = getHighestBuyPrice(orderBook);
		return altCoinBalance * highestBuy;
	}


	/**
	 * this represents how many alt coins we can buy given a BTC balance in the
	 * account
	 */	
	@Override
	public Double convertBalanceToAltCoin(Double btcBalance, String coinType,
			Double price) {
		return btcBalance
				/ (price * (1 + configs.minPercentRangeToTrade()));
	}

	public void setConfigs(ConfigurationsServiceImpl configs) {
		this.configs = configs;
	}

}
