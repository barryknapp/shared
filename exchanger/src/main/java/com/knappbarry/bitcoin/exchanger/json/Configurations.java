package com.knappbarry.bitcoin.exchanger.json;

public class Configurations {


	public String getCoinForExchange(String exchange) {
		return exchange.replace("_btc", "");
	}

	public String getBaseApiUrl() {
		return "http://data.bter.com/api/1";
	}

	public Double getPressureDifferenceToTriggerTrade() {
		return new Double(1.03);
	}

	public String getUserId() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getPassword() {
		// TODO Auto-generated method stub
		return null;
	}

	public Double getTierPercentage(int tier) {

		switch (tier) {
		case (1):
			return new Double(1);
		case (2):
			return new Double(2);
		case (3):
			return new Double(3);
		}
		return new Double(3);
	}

	public int getTierWeightedDivisor(int tier) {
		return tier;
	}

	public int getNumberOfTiersToEvaluate() {
		return 3;
	}

	/**
	 * when executing a trade, this is the minimum percent within range of the
	 * buy or sell price if we want to turn a 2% profit, we should trade > 2%
	 * ranges
	 * 
	 * @return
	 */
	public Double minPercentRangeToTrade() {
		return new Double(1.5);
	}

}
