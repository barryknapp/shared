package com.knappbarry.bitcoin.exchanger.clients;

import java.math.BigDecimal;
import java.util.Map;

import com.knappbarry.bitcoin.exchanger.model.BlockchainInfoCurrency;

public interface BlockchainInfoClient {

	Map<String, BlockchainInfoCurrency> getTicker() throws Exception;

	BigDecimal toBTC(String currency, BigDecimal value) throws Exception;

}
