
package com.knappbarry.bitcoin.exchanger.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.knappbarry.bitcoin.exchanger.dao.BtcBalanceDao;
import com.knappbarry.bitcoin.exchanger.dao.CompleteOrderDao;
import com.knappbarry.bitcoin.exchanger.model.BtcBalance;
import com.knappbarry.bitcoin.exchanger.model.CompleteOrder;
import com.knappbarry.bitcoin.exchanger.service.ExchangerService;
import com.knappbarry.bitcoin.exchanger.service.impl.ConfigurationsServiceImpl;

@RestController
public class AccountController {

	@Autowired
	ExchangerService serviceBittrex;

	@Autowired
	BtcBalanceDao btcBalanceDao;

	@Autowired
	ConfigurationsServiceImpl configurationsService;

	@Autowired
	CompleteOrderDao completeOrderDao;

	static Logger logger = LogManager.getLogger(AccountController.class);

	@RequestMapping("/status")
	public Iterable<CompleteOrder> greeting(@RequestParam(value = "accountKey", required = true) String accountKey) {

		return completeOrderDao.findByAccountKey(accountKey);
	}

	@RequestMapping("/restart")
	public String restart(@RequestParam(value = "accountKey", required = true) String accountKey) {
		try {
			serviceBittrex.checkForTradeOpportunities(true, accountKey);
			return "RESTART COMPLETED FOR " + accountKey;
		} catch (Exception e) {
			logger.error("Restart failed ", e);
			return "RESTART FAILED";
		}
	}

	@RequestMapping("/balances")
	public Iterable<BtcBalance> balances(@RequestParam(value = "accountKey", required = false) String accountKey) {
		try {
			if (accountKey == null) {
				List<Iterable<BtcBalance>> balances = configurationsService.getAccounts().stream()
						.map(s -> getBalance(s)).collect(Collectors.toList());
				List<BtcBalance> allBalances = new ArrayList<BtcBalance>();
				for (Iterable<BtcBalance> balancesItr : balances) {
					for (BtcBalance bal : balancesItr) {
						allBalances.add(bal);
					}
				}
				return allBalances;
			}
			return getBalance(accountKey);
		} catch (Exception e) {
			logger.error("balances failed ", e);
			return null;
		}
	}

	private Iterable<BtcBalance> getBalance(String accountKey) {
		Date maxDate = btcBalanceDao.findLatestBalanceEntryDateForAccountOwner(accountKey);
		return btcBalanceDao.findByAccountOwnerAndCreateTs(accountKey, maxDate);
	}

}
