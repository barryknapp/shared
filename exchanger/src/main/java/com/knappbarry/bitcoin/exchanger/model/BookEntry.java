package com.knappbarry.bitcoin.exchanger.model;

import java.util.Date;
import java.util.List;

public class BookEntry {

	public enum ENTRY_TYPE {bid, ask};
	
	private Date createTs;
	private ENTRY_TYPE type;
	private List<OpenOrder> bids;
	private List<OpenOrder> asks;
	public ENTRY_TYPE getType() {
		return type;
	}
	public void setType(ENTRY_TYPE type) {
		this.type = type;
	}
	public List<OpenOrder> getBids() {
		return bids;
	}
	public void setBids(List<OpenOrder> bids) {
		this.bids = bids;
	}
	public List<OpenOrder> getAsks() {
		return asks;
	}
	public void setAsks(List<OpenOrder> asks) {
		this.asks = asks;
	}
	public Date getCreateTs() {
		return createTs;
	}
	public void setCreateTs(Date createTs) {
		this.createTs = createTs;
	}
	@Override
	public String toString() {
		return "BookEntry [createTs=" + createTs + ", type=" + type + ", bids="
				+ bids + ", asks=" + asks + "]";
	}
	
	
	
}
