package com.knappbarry.bitcoin.exchanger.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.knappbarry.bitcoin.exchanger.service.RestConnectionService;

public class RestConnectionServiceImpl implements RestConnectionService {

	Logger logger = LogManager.getLogger(RestConnectionServiceImpl.class);
	ConfigurationsServiceImpl configs;

	@Override
	public String connect(String path, String httpVerb, Map<String, String> postParams, String strategy)
			throws Exception {

		String url = configs.getBaseApiUrl() + path;
		try {
			return doRequest(url, httpVerb, postParams, strategy);
		} catch (Exception e) {
			logger.warn("Error calling url " + url);
			throw e;
		}
	}

	@Override
	public String connect(String path, String httpVerb, String strategy) throws Exception {
		return connect(path, httpVerb, new HashMap<String, String>(), strategy);

	}

	public void setConfigs(ConfigurationsServiceImpl configs) {
		this.configs = configs;
	}

	public String doRequest(String url, String requestType, Map<String, String> arguments, String strategy)
			throws Exception {

		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();

		Mac mac = null;
		SecretKeySpec key = null;

		String postData = "";

		for (Iterator<Entry<String, String>> argumentIterator = arguments.entrySet().iterator(); argumentIterator
				.hasNext();) {

			Map.Entry<String, String> argument = argumentIterator.next();

			urlParameters.add(new BasicNameValuePair(argument.getKey().toString(), argument.getValue().toString()));

			if (postData.length() > 0) {
				postData += "&";
			}

			postData += argument.getKey() + "=" + argument.getValue();

		}

		// Create a new secret key
		try {
			key = new SecretKeySpec(configs.getSecret(strategy).getBytes("UTF-8"), "HmacSHA512");
		} catch (UnsupportedEncodingException uee) {
			logger.error("Unsupported encoding exception: " + uee.toString(), uee);
		}

		// Create a new mac
		try {
			mac = Mac.getInstance("HmacSHA512");
		} catch (Exception nsae) {
			logger.error("No such algorithm exception: " + nsae.toString(), nsae);
		}

		// Init mac with key.
		try {
			mac.init(key);
		} catch (Exception ike) {
			logger.error("Invalid key exception: " + ike.toString(), ike);
		}

		// add header
		Header[] headers = new Header[2];
		headers[0] = new BasicHeader("Key", configs.getKey(strategy));
		headers[1] = new BasicHeader("Sign", Hex.encodeHexString(mac.doFinal(postData.getBytes("UTF-8"))));

		// Now do the actual request

		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = null;
		HttpGet get = null;
		HttpResponse response = null;

		if (requestType == "POST") {
			post = new HttpPost(url);

			post.setEntity(new UrlEncodedFormEntity(urlParameters));
			post.setHeaders(headers);
			response = client.execute(post);
		} else if (requestType == "GET") {
			get = new HttpGet(url);
			get.setHeaders(headers);
			response = client.execute(get);
		}
		InputStreamReader isReader = new InputStreamReader(response.getEntity().getContent());
		BufferedReader rd = new BufferedReader(isReader);

		StringBuffer buffer = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			buffer.append(line);
		}

		isReader.close();

		logger.debug("Sending '" + requestType + "' request to URL : " + url);
		if (post != null) {
			logger.debug("Post parameters : " + post.getEntity());
		}
		logger.debug("Response Code : " + response.getStatusLine().getStatusCode());

		String resp = buffer.toString();

		if (post != null) {
			post.completed();
			post.releaseConnection();
		} else if (get != null) {
			get.completed();
			get.releaseConnection();
		}

		logger.debug("RestResponse: " + resp);
		return resp;

	}
}
