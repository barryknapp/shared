package com.knappbarry.bitcoin.exchanger.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.knappbarry.bitcoin.exchanger.model.BtcBalance;

public interface BtcBalanceDao  extends CrudRepository<BtcBalance, String> {

	@Query("select max(createTs) from BtcBalance where accountOwner =  ?1")
	public Date findLatestBalanceEntryDateForAccountOwner(String accountOwner);
	
	@Query("select min(createTs) from BtcBalance where accountOwner =  ?1 and createTs > ?2")
	public Date findMinBalanceEntryDateForAccountOwnerAfterDate(String accountOwner, Date minDate);

	public List<BtcBalance> findByAccountOwnerAndCreateTs(String accountOwner, Date lastRestartDate);

}
