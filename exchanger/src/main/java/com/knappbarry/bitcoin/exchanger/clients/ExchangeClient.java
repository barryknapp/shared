package com.knappbarry.bitcoin.exchanger.clients;

import java.util.List;
import java.util.Map;

import com.knappbarry.bitcoin.exchanger.model.AccountConfiguration;
import com.knappbarry.bitcoin.exchanger.model.AvailableFunds;
import com.knappbarry.bitcoin.exchanger.model.BookEntry;
import com.knappbarry.bitcoin.exchanger.model.OrderResponse;
import com.knappbarry.bitcoin.exchanger.model.OrderStatus;

public interface ExchangeClient {

	AvailableFunds getAvailableFunds(String strategy, final AccountConfiguration accountConfig) throws Exception;

	OrderStatus cancelOrder(String orderid, String strategy, final AccountConfiguration accountConfig) throws Exception;

	OrderStatus getSubmittedOrder(String orderid, String strategy, final AccountConfiguration accountConfig) throws Exception;

	OrderResponse placeOrder(Map<String, String> params, String strategy, final AccountConfiguration accountConfig) throws Exception;

	BookEntry getOrderBook(String coinExchange, String strategy, final AccountConfiguration accountConfig) throws Exception;

	String getWebsiteName();

	List<OrderStatus> getOrderHistory(final AccountConfiguration accountConfig);

	List<OrderStatus> getOpenOrders(final AccountConfiguration accountConfig);

}
