package com.knappbarry.bitcoin.exchanger.service;

public interface ExchangerService {

	void checkForTradeOpportunities(boolean restart) throws Exception;

	void checkForTradeOpportunities(boolean restart, String accountKey) throws Exception;

}
