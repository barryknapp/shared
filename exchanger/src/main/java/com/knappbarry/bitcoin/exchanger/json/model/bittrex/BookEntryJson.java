package com.knappbarry.bitcoin.exchanger.json.model.bittrex;

import java.util.List;

import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;

@AutoProperty
public class BookEntryJson {

	boolean success;
	String message;

	private List<OpenOrderJson> buy;
	private List<OpenOrderJson> sell;

	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public List<OpenOrderJson> getSell() {
		return sell;
	}

	public void setSell(List<OpenOrderJson> sell) {
		this.sell = sell;
	}

	public List<OpenOrderJson> getBuy() {
		return buy;
	}

	public void setBuy(List<OpenOrderJson> buy) {
		this.buy = buy;
	}

	@Override
	public String toString() {
		return Pojomatic.toString(this);
	}

	@Override
	public int hashCode() {
		return Pojomatic.hashCode(this);
	}

	@Override
	public boolean equals(Object o) {
		return Pojomatic.equals(this, o);
	}

}
